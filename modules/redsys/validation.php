<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/redsys.php');
Tools::displayFileAsDeprecated();

if (!empty($_POST))
{
	/** Recoger datos de respuesta **/
	$total     = Tools::getValue('Ds_Amount');
	$pedido    = Tools::getValue('Ds_Order');
	$codigo    = Tools::getValue('Ds_MerchantCode');
	$moneda    = Tools::getValue('Ds_Currency');
	$respuesta = Tools::getValue('Ds_Response');
	$firma_remota = Tools::getValue('Ds_Signature');
    $transaction_id = ltrim(Tools::getValue('Ds_Order'), '0');;

	/** Clave **/
	$clave = Configuration::get('REDSYS_CLAVE');

	/** SHA1 **/
	$mensaje = $total.$pedido.$codigo.$moneda.$respuesta.$clave;
	$firma_local = Tools::strtoupper(sha1($mensaje));

	if ($firma_local == $firma_remota)
	{
		/** Creamos los objetos para confirmar el pedido **/
		$pedido = Tools::substr($pedido, 0, 10);
		$context = Context::getContext();
		$cart = new Cart($pedido);
		$redsys = new redsys();
		if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$redsys->active)
			Tools::redirect('index.php?controller=order&step=1');
		/** Objeto cliente **/
		$customer = new Customer((int)$cart->id_customer);
		if (!Validate::isLoadedObject($customer))
			Tools::redirect('index.php?controller=order&step=1');

		/** Formatear variables **/
		$total = number_format($total / 100, 2, '.', '');
		$respuesta = (int)$respuesta;
		if ($respuesta < 101)
		{
			/** Compra válida **/
			$mailvars = array('transaction_id' => $transaction_id);
			$redsys->validateOrder($pedido, _PS_OS_PAYMENT_, $total, $redsys->displayName, null, $mailvars, (int)$cart->id_currency, false, $customer->secure_key);
		}
		else
			/** se anota el pedido como no pagado **/
			$redsys->validateOrder($pedido, _PS_OS_ERROR_, 0, $redsys->displayName, 'errores:'.$respuesta);
	}
	else
		/** se anota el pedido como no pagado **/
		$redsys->validateOrder($pedido, _PS_OS_ERROR_, 0, $redsys->displayName, 'errores:'.$respuesta);
}
else if (!empty($_GET))
{
	/** Recoger datos de respuesta **/
	$total     = Tools::getValue('Ds_Amount');
	$pedido    = Tools::getValue('Ds_Order');
	$codigo    = Tools::getValue('Ds_MerchantCode');
	$moneda    = Tools::getValue('Ds_Currency');
	$respuesta = Tools::getValue('Ds_Response');
	$firma_remota = Tools::getValue('Ds_Signature');

	/** Clave **/
	$clave = Configuration::get('REDSYS_CLAVE');

	/** SHA1 **/
	$mensaje = $total.$pedido.$codigo.$moneda.$respuesta.$clave;
	$firma_local = Tools::strtoupper(sha1($mensaje));

	if ($firma_local == $firma_remota)
	{
		/** Formatear variables **/
		$respuesta = (int)$respuesta;
		if ($respuesta < 101)
		{
			/** Compra válida **/
			Tools::redirect('index.php');
		}
		else
			Tools::redirect('index.php');
	}
	else
		Tools::redirect('index.php');
}
?>