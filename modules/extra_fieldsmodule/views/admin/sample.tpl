    <div id="product-customization" class="panel product-tab">
        <input type="hidden" name="submitted_tabs[]" value="Customization" />
        <h3>{l s='Extra Fields Module' mod='extra_fieldsmodule'}</h3>

        <div class="form-group">
            <div class="col-lg-1"><span class="pull-right"></span></div>
            <label class="control-label col-lg-3" for="extra_fields">
                <span class="label-tooltip" data-toggle="tooltip"
                      title="{l s='Recomed price.'}">
				{l s='Recomed price'}
			</span>
            </label>
            <div class="col-lg-1">
                <input type="text" name="extra_fields" id="extra_fields" value="{if isset($belvg_textarea)}{$belvg_textarea}{/if}" />
            </div>
        </div>
        <div class="panel-footer">
            <a href="{$link->getAdminLink('AdminProducts')|escape:'html':'UTF-8'}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel'}</a>
            <button type="submit" name="submitAddproduct" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save'}</button>
            <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right" disabled="disabled"><i class="process-icon-loading"></i> {l s='Save and stay'}</button>
        </div>
    </div>