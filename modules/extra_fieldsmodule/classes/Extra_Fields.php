<?php

class Extra_Fields extends ObjectModel
{
	/** @var string Name */
	public $id_extra_fieldsmodule;
		
	/** @var integer */
	public $id_product;
	
	/** @var integer */
	public $textarea;
	
    /**
     * @see ObjectModel::$definition
     */
    public static $definition = array(
        'table' => 'extra_fieldsmodule',
        'primary' => 'id_extra_fieldsmodule',
        'multilang' => FALSE,
        'fields' => array(
            'id_product' => array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => TRUE),
            'textarea' => array('type' => self::TYPE_HTML, 'validate' => 'isString'),
        ),
    );
	
    public static function loadByIdProduct($id_product){
        $result = Db::getInstance()->getRow('
            SELECT *
            FROM `'._DB_PREFIX_.'extra_fieldsmodule` sample
            WHERE sample.`id_product` = '.(int)$id_product
        );
        
        return new Extra_Fields($result['id_extra_fieldsmodule']);
    }
}

