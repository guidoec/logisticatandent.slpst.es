<?php

if (!defined('_PS_VERSION_')) {
    exit;
}

require_once(dirname(__FILE__) . '/classes/Extra_Fields.php');

class Extra_FieldsModule extends Module {

    public function __construct() {
        $this->name = 'extra_fieldsmodule';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Holets';
        $this->need_instance = 0;
        $this->module_key = "";

        parent::__construct();

        $this->displayName = $this->l('Extra Fields Module');
        $this->description = $this->l('Works with displayAdminProductsExtra and actionProductUpdate');
    }

    public function install() {
        $sql = array();
	
        $sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'extra_fieldsmodule` (
                  `id_extra_fieldsmodule` int(10) unsigned NOT NULL AUTO_INCREMENT,
                  `id_product` INT( 11 ) UNSIGNED NOT NULL,
                  `textarea` TEXT NOT NULL,
                  PRIMARY KEY (`id_extra_fieldsmodule`),
                  UNIQUE  `BELVG_SAMPLE_UNIQ` (  `id_product` )
                ) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
                                
        if (!parent::install() OR 
            !$this->registerHook('displayAdminProductsExtra') OR
            !$this->registerHook('actionProductUpdate') OR
            !$this->registerHook('displayFooterProduct') OR
			!Configuration::updateValue('sample_module_textarea', '') OR
            !$this->runSql($sql)
        ) {
            return FALSE;
        }
        
        return TRUE;
    }
    
    public function uninstall() {
        $sql = array();
	
        $sql[] = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'extra_fieldsmodule`';
        if (!parent::uninstall() OR
            !$this->runSql($sql) 
        ) {
            return FALSE;
        }

        return TRUE;
    }
    
    public function runSql($sql) {
        foreach ($sql as $s) {
			if (!Db::getInstance()->Execute($s)){
				return FALSE;
			}
        }
        
        return TRUE;
    }
    
    public function hookDisplayAdminProductsExtra($params) {
        $id_product = Tools::getValue('id_product');
        $sampleObj = Extra_Fields::loadByIdProduct($id_product);
        if(!empty($sampleObj) && isset($sampleObj->id)){
            $this->context->smarty->assign(array(
                'belvg_textarea' => $sampleObj->textarea,
            ));
        }
        
        return $this->display(__FILE__, 'views/admin/sample.tpl');
    }
    
    public function hookActionProductUpdate($params) {
        $id_product = Tools::getValue('id_product');
        $sampleObj = Extra_Fields::loadByIdProduct($id_product);
        $sampleObj->textarea = Tools::getValue('extra_fields');
        $sampleObj->id_product = $id_product;
        
        if(!empty($sampleObj) && isset($sampleObj->id)){
            $sampleObj->update();
        } else {
            $sampleObj->add();
        }
    }
    /*
    public function hookDisplayFooterProduct($params) {
        $id_product = Tools::getValue('id_product');
        $sampleObj = Extra_Fields::loadByIdProduct($id_product);
        if(!empty($sampleObj) && isset($sampleObj->id)){
            $this->context->smarty->assign(array(
                'belvg_textarea' => $sampleObj->textarea,
            ));
        }
        
        return $this->display(__FILE__, 'views/frontend/sample.tpl');
    }
    */
    
}