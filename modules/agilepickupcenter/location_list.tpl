    <p id="id_loc_message">
    </p>
    <fieldset>
    <legend style="padding:5px 5px 5px 5px;font-size:16px;">{l s=' pickup locations:' mod='agilepickupcenter'}</legend>
    <p style="padding-bottom: 20px; width: 38%; float: left">
    {l s='Distance scope:' mod='agilepickupcenter'} <select id="distance_scope" name="distance_scope" style="width:250px">
        <option value="2" {if $distance_range==2}selected{/if}> {l s='Less than 2 KM' mod='agilepickupcenter'}</option>
        <option value="5" {if $distance_range==5}selected{/if}> {l s='Less than 5 KM' mod='agilepickupcenter'}</option>
        <option value="10" {if $distance_range==10}selected{/if}> {l s='Less than 10 KM' mod='agilepickupcenter'}</option>
        <option value="15" {if $distance_range==15}selected{/if}>{l s='Less than 15 KM' mod='agilepickupcenter'}</option>
        <option value="20" {if $distance_range==20}selected{/if}>{l s='Less than 20 KM' mod='agilepickupcenter'}</option>
        <option value="30" {if $distance_range==30}selected{/if}> {l s='Less than 30 KM' mod='agilepickupcenter'}</option>
        <option value="50" {if $distance_range==50}selected{/if}> {l s='Less than 50 KM' mod='agilepickupcenter'}</option>
        <option value="80" {if $distance_range==80}selected{/if}> {l s='Less than 80 KM' mod='agilepickupcenter'}</option>
        <option value="1000000">{l s='All locations' mod='agilepickupcenter'}</option>
    </select>
        <div style="float: left; font-weight: bold; padding-left: 0px;">&#65513; <span style="color: red">¿Muchas farmacias?</span> Reduce el rango de busqueda.</div>
    </p>

    <table width="100%" id="table_agilecarrier" class="table-agile">
        <thead class="table-head" ">
        <tr><td><strong>{l s='ID' mod='agilepickupcenter'}</strong></td>
        <td><strong>{l s='Location' mod='agilepickupcenter'}</strong></td>
        <td><strong>{l s='City' mod='agilepickupcenter'}</strong></td>
        <td><strong>{l s='Phone' mod='agilepickupcenter'}</strong></td>
        <td><strong>{l s='Distance' mod='agilepickupcenter'}</strong></td></tr>
        </thead>
        <tbody class="table-body">
        {foreach from=$locations item=location key=index name=count}
            <tr id="tr_location_{$location.id_location}">
            <td>{$smarty.foreach.count.index+1}<input type="radio" name="id_location" id="id_location_{$location.id_location}" value="{$location.id_location}" {if $sl_id_location==$location.id_location} checked {/if}/></td>
            <td>{$location.location}</td><td>{$location.city}</td>
            <td>{$location.phone}</td><td id="td_{$location.id_location}" alt="{$location.id_location}">{$location.distance} KM</td>
            </tr>
        {/foreach}
        </tbody>
    </table>
    <div id="location_list">
    </div>
    </fieldset>
            
    <script type="text/javascript">
        {foreach from=$locations item='location'}
            latlngLocations[{$location.id_location}] = new google.maps.LatLng({$location.latitude}, {$location.longitude});
            indexLocations[{$location.id_location}] = {$location.index};
        {/foreach}
        
        doInitalizationMap();
        
        $("select#distance_scope").change(function() 
        {ldelim}
            get_location_list($("select#distance_scope").val());            
            get_location_desc($("select#distance_scope").val());
            doInitalizationMap();
        {rdelim}
        );

        $("input[id^='id_location_']").click(function() 
        {ldelim}
            sl_id_location = $(this).val();
            showMarkerDetail(sl_id_location);
            get_location_desc($("select#distance_scope").val());
			update_payment_methods();
			
        {rdelim}
        );

	function update_payment_methods()
	{
		if ($('#cgv:checked').length != 0)
			var checked = 1;
		else
			var checked = 0;

		var data = 'ajax=true&method=updateTOSStatusAndGetPayments&checked=' + checked + '&token=' + static_token;
		$('#opc_payment_methods-overlay').fadeIn('slow');

		if(typeof(orderOpcUrl) == "undefined")return;

		$.ajax({
			type: 'POST',
			url: orderOpcUrl,
			data: data,
			cache: false,
			dataType : "json",
			success: function(json) 
			{
				$('div#HOOK_TOP_PAYMENT').html(json.HOOK_TOP_PAYMENT);
				$('#opc_payment_methods-content div#HOOK_PAYMENT').html(json.HOOK_PAYMENT);
				$('#opc_payment_methods-overlay').fadeOut('slow');	
			}
		});
	}
		
    </script>        
