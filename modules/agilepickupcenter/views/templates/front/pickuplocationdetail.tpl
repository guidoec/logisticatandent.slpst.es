{capture name=path}<a href="{$link->getPageLink('my-account', true)}">{l s='My Account' mod='agilepickupcenter'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My Seller Account'  mod='agilepickupcenter'}{/capture}
<script type="text/javascript" src="{$theme_js_dir_ssl}tools/statesManagement.js"></script>
<script type="text/javascript">
    idSelectedCountry = {if isset($smarty.post.id_state)}{$smarty.post.id_state|intval}{else}{if isset($location->id_state)}{$location->id_state|intval}{else}false{/if}{/if};
	{if isset($countries)}
		{addJsDef countries=$countries}
	{/if}


	$(document).ready(function() {
		$("input#allwkday_on").click(function() {
		var $radios = $("input:radio[name^='weekday']");
			if ($radios.is(":checked") === false) {
				$radios.filter("[value=0]").attr("checked", true);
			}
			else {
				$radios.filter("[value=1]").attr("checked", true);
			}
		});
		$("input#allwkday_off").click(function() {
			var $radios = $("input:radio[name^='weekday']");
			if ($radios.is(":checked") === true) {
				$radios.filter("[value=0]").attr("checked", true);
			}
			else {
				$radios.filter("[value=1]").attr("checked", true);
			}
		});
		$("input#allwkday_from").change(function() {
			var val = $("input#allwkday_from").val();
			$("input#weekday1_from").val(val);
			$("input#weekday2_from").val(val);
			$("input#weekday3_from").val(val);
			$("input#weekday4_from").val(val);
			$("input#weekday5_from").val(val);
			$("input#weekday6_from").val(val);
			$("input#weekday7_from").val(val);
		});
		$("input#allwkday_to").change(function() {
			var val = $("input#allwkday_to").val();
			$("input#weekday1_to").val(val);
			$("input#weekday2_to").val(val);
			$("input#weekday3_to").val(val);
			$("input#weekday4_to").val(val);
			$("input#weekday5_to").val(val);
			$("input#weekday6_to").val(val);
			$("input#weekday7_to").val(val);
		});
	});

</script>

<style>
.margin-form {
	padding: 0 0 1em 260px;
	color: #7F7F7F;
	font-size: 1.2em;
}


.margin-form .normal-text {
	display: block;
	padding-top: 0.2em;
	font-size: 1.2em;
	color: black;
}

fieldset.width1 .margin-form {
	margin: 0 0 1em 130px;
}

#fieldset_0 label {
	float: left;
	width: 250px;
	padding: 0.2em 0.5em 0 0;
	text-align: right;
	font-weight: bold;
}
#fieldset_0 label.t {
	float: none;
	clear: none;
	padding: 0 5px 0;
	margin: 0px;
	font-weight: normal;
	font-size: 12px;
	text-shadow:none;
}
#fieldset_0 label.child {
	float: none;
	clear: none;
	padding: 0px;
	margin: 0px;
	font-weight: normal;
	font-size: 12px;
}
#fieldset_0 label.std {
	float: none;
	margin: 0px;
	width: 170px;
	display: block;
	font-size: 12px;
	text-align: right;
	font-weight: bold;
}
</style>
<h1>{l s='My Seller Account' mod='agilepickupcenter'}</h1>
{include file="$tpl_dir./errors.tpl"}

{include file="$agilemultipleseller_views./templates/front/seller_tabs.tpl"}
{if isset($isSeller) AND $isSeller}
	{if isset($isSeller) AND $isSeller}
		<form action="{$link->getModuleLink('agilepickupcenter', 'pickuplocationdetail', ['process' =>'detail','id_location'=>$id_location], true)}" enctype="multipart/form-data" method="post" class="std">
			<div class="margin-form">
				<span style="float:right;padding:0px 30px 0px 0px">
					<a href="{$link->getModuleLink('agilepickupcenter', 'pickuplocations', ['process' =>'list'], true)}">{l s='Back to list' mod='agilepickupcenter'}</a>
				</span>
			</div>
			<div>&nbsp;</div>
			<fieldset id="fieldset_0">
				<legend>
					<img src="{$base_dir_ssl}img/admin/tab-customers.gif" alt="Seller Info">Seller Info
				</legend>
				<input name="id_seller" value="{$location->id_seller}" type="hidden">
				<input name="id_carrier" value="{$location->id_carrier}" type="hidden">
				<label>{l s='Location:' mod='agilepickupcenter'}<sup>*</sup></label>
				<div class="margin-form">
					<input name="location" id="location" value="{$location->location}" class="" size="33" type="text">
				</div>

				<label>{l s='Status:' mod='agilepickupcenter'}</label>
				<div class="margin-form">
					<input name="active" id="active_on" value="1" type="radio" {if $location->active == 1} checked="checked" {/if}>
					<label class="t" for="active_on">
					<img src="{$base_dir_ssl}/img/admin/enabled.gif" alt="Enabled" title="Enabled">
					</label>
					<input name="active" id="active_off" value="0" type="radio" {if $location->active == 0} checked="checked" {/if}>
					<label class="t" for="active_off">
					<img src="{$base_dir_ssl}/img/admin/disabled.gif" alt="Disabled" title="Disabled">
					</label>
				</div>

				<label>{l s='Email Address:' mod='agilepickupcenter'}</label>
				<div class="margin-form">
					<input name="email" id="email" value="{$location->email}" class="" size="33" type="text">
				</div>

				<label>{l s='Address Line1:' mod='agilepickupcenter'}<sup>*</sup></label>
				<div class="margin-form">
					<input name="address1" id="address1" value="{$location->address1}"  size="33" type="text">
				</div>
				<label>{l s='Address Line2:' mod='agilepickupcenter'}</label>
				<div class="margin-form">
					<input name="address2" id="address2" value="{$location->address2}"  size="33" type="text">
				</div>

				<label>{l s='City:' mod='agilepickupcenter'}<sup>*</sup></label>
				<div class="margin-form">
					<input name="city" id="city" value="{$location->city}" size="33" type="text">
				</div>

				<label>{l s='Postal Code:' mod='agilepickupcenter'}<sup>*</sup></label>
				<div class="margin-form">
					<input name="postcode" id="postcode" value="{$location->postcode}" size="33" type="text">
				</div>
				{* state, country *}
				<label>{l s='Country:' mod='agilepickupcenter'}<sup>*</sup></label>
				<div class="margin-form">
					<select id="id_country" name="id_country">
						{foreach from=$countries item=country}
							<option value="{$country.id_country}" 
								{if isset($smarty.post.id_country)}
									{if $smarty.post.id_country == $country.id_country}selected="selected"{/if}
								{else}
									{if $location->id_country == $country.id_country}selected="selected"{/if}
								{/if}>{$country.name|escape:'htmlall':'UTF-8'}
							</option>
						{/foreach}
					</select>
				</div>
				<label class="id_state">{l s='State:' mod='agilepickupcenter'}</label>
				<div class="margin-form id_state">
					<select name="id_state" id="id_state">
				            <option value="">-</option>
					</select>
				</div>

				<label>{l s='Phone:' mod='agilepickupcenter'}</label>
				<div class="margin-form">
					<input name="phone" id="phone" value="{$location->phone}" size="33" type="text">
				</div>

				<label>{l s='Fax:' mod='agilepickupcenter'}</label>
				<div class="margin-form">
					<input name="fax" id="fax" value="{$location->fax}" size="33" type="text">
				</div>

				<label>{l s='Description:' mod='agilepickupcenter'}</label>
				<div class="margin-form">
					<textarea name="description" id="description" cols="94" rows="5">{$location->description}</textarea>
				</div>

				<label>Operation Info: </label>
				<div class="margin-form">
					<table>
					<thead><tr>
						<th align="center">Mon</th>
						<th align="center">Tue</th>
						<th align="center">Wed</th>
						<th align="center">Thu</th>
						<th align="center">Fri</th>
						<th align="center">Sat</th>
						<th align="center">Sun</th>
						<th align="center">All</th>
					</tr></thead>
					<tbody><tr>
						<td align="center">
						<input name="weekday1" id="weekday1_on" value="1" type="radio" {if $location->weekday1 == 1} checked="checked" {/if}>
						<label class="t" for="weekday1_on"><img src="{$base_dir_ssl}/img/admin/enabled.gif" alt="Open" title="Open"></label><br>
						<input name="weekday1" id="weekday1_off" value="0" type="radio" {if $location->weekday1 == 0} checked="checked" {/if}>
						<label class="t" for="weekday1_off"><img src="{$base_dir_ssl}/img/admin/disabled.gif" alt="Close" title="Close"></label>
						</td>
						<td align="center">
						<input name="weekday2" id="weekday2_on" value="1" type="radio" {if $location->weekday2 == 1} checked="checked" {/if}>
						<label class="t" for="weekday2_on"><img src="{$base_dir_ssl}/img/admin/enabled.gif" alt="Open" title="Open"></label><br>
						<input name="weekday2" id="weekday2_off" value="0" type="radio" {if $location->weekday2 == 0} checked="checked" {/if}>
						<label class="t" for="weekday2_off"><img src="{$base_dir_ssl}/img/admin/disabled.gif" alt="Close" title="Close"></label>
						</td>
						<td align="center">
						<input name="weekday3" id="weekday3_on" value="1" type="radio" {if $location->weekday3 == 1} checked="checked" {/if}>
						<label class="t" for="weekday3_on"><img src="{$base_dir_ssl}/img/admin/enabled.gif" alt="Open" title="Open"></label><br>
						<input name="weekday3" id="weekday3_off" value="0" type="radio" {if $location->weekday3 == 0} checked="checked" {/if}>
						<label class="t" for="weekday3_off"><img src="{$base_dir_ssl}/img/admin/disabled.gif" alt="Close" title="Close"></label>
						</td>
						<td align="center">
						<input name="weekday4" id="weekday4_on" value="1" type="radio" {if $location->weekday4 == 1} checked="checked" {/if}>
						<label class="t" for="weekday4_on"><img src="{$base_dir_ssl}/img/admin/enabled.gif" alt="Open" title="Open"></label><br>
						<input name="weekday4" id="weekday4_off" value="0" type="radio" {if $location->weekday4 == 0} checked="checked" {/if}>
						<label class="t" for="weekday4_off"><img src="{$base_dir_ssl}/img/admin/disabled.gif" alt="Close" title="Close"></label>
						</td>
						<td align="center">
						<input name="weekday5" id="weekday5_on" value="1" type="radio" {if $location->weekday5 == 1} checked="checked" {/if}>
						<label class="t" for="weekday5_on"><img src="{$base_dir_ssl}/img/admin/enabled.gif" alt="Open" title="Open"></label><br>
						<input name="weekday5" id="weekday5_off" value="0" type="radio" {if $location->weekday5 == 0} checked="checked" {/if}>
						<label class="t" for="weekday5_off"><img src="{$base_dir_ssl}/img/admin/disabled.gif" alt="Close" title="Close"></label>
						</td>
						<td align="center">
						<input name="weekday6" id="weekday6_on" value="1" type="radio" {if $location->weekday6 == 1} checked="checked" {/if}>
						<label class="t" for="weekday6_on"><img src="{$base_dir_ssl}/img/admin/enabled.gif" alt="Open" title="Open"></label><br>
						<input name="weekday6" id="weekday6_off" value="0" type="radio" {if $location->weekday6 == 0} checked="checked" {/if}>
						<label class="t" for="weekday6_off"><img src="{$base_dir_ssl}/img/admin/disabled.gif" alt="Close" title="Close"></label>
						</td>
						<td align="center">
						<input name="weekday7" id="weekday7_on" value="1" type="radio" {if $location->weekday7 == 1} checked="checked" {/if}>
						<label class="t" for="weekday7_on"><img src="{$base_dir_ssl}/img/admin/enabled.gif" alt="Open" title="Open"></label><br>
						<input name="weekday7" id="weekday7_off" value="0" type="radio" {if $location->weekday7 == 0} checked="checked" {/if}>
						<label class="t" for="weekday7_off"><img src="{$base_dir_ssl}/img/admin/disabled.gif" alt="Close" title="Close"></label>
						</td>
			    			 <td align="center">
				    		 <input name="allwkday" id="allwkday_on" value="1" type="radio">
						 <label class="t" for="allwkday_on"><img src="{$base_dir_ssl}/img/admin/enabled.gif" alt="Open" title="Open"></label><br>
						 <input name="allwkday" id="allwkday_off" value="0" type="radio">
						 <label class="t" for="allwkday_off"><img src="{$base_dir_ssl}/img/admin/disabled.gif" alt="Close" title="Close"></label>
						 </td>
					</tr>
					<tr>
						<td><input name="weekday1_from" id="weekday1_from" maxlength="5" size="5" value="{substr($location->weekday1_from,0,5)}" type="text"></td>
						<td><input name="weekday2_from" id="weekday2_from" maxlength="5" size="5" value="{substr($location->weekday2_from,0,5)}" type="text"></td>
						<td><input name="weekday3_from" id="weekday3_from" maxlength="5" size="5" value="{substr($location->weekday3_from,0,5)}" type="text"></td>
						<td><input name="weekday4_from" id="weekday4_from" maxlength="5" size="5" value="{substr($location->weekday4_from,0,5)}" type="text"></td>
						<td><input name="weekday5_from" id="weekday5_from" maxlength="5" size="5" value="{substr($location->weekday5_from,0,5)}" type="text"></td>
						<td><input name="weekday6_from" id="weekday6_from" maxlength="5" size="5" value="{substr($location->weekday6_from,0,5)}" type="text"></td>
						<td><input name="weekday7_from" id="weekday7_from" maxlength="5" size="5" value="{substr($location->weekday7_from,0,5)}" type="text"></td>
						<td><input name="allwkday_from" id="allwkday_from" maxlength="5" size="5" type="text"></td>
					</tr>
					<tr>
						<td><input name="weekday1_to" id="weekday1_to" maxlength="5" size="5" value="{substr($location->weekday1_to,0,5)}" type="text"></td>
						<td><input name="weekday2_to" id="weekday2_to" maxlength="5" size="5" value="{substr($location->weekday2_to,0,5)}" type="text"></td>
						<td><input name="weekday3_to" id="weekday3_to" maxlength="5" size="5" value="{substr($location->weekday3_to,0,5)}" type="text"></td>
						<td><input name="weekday4_to" id="weekday4_to" maxlength="5" size="5" value="{substr($location->weekday4_to,0,5)}" type="text"></td>
						<td><input name="weekday5_to" id="weekday5_to" maxlength="5" size="5" value="{substr($location->weekday5_to,0,5)}" type="text"></td>
						<td><input name="weekday6_to" id="weekday6_to" maxlength="5" size="5" value="{substr($location->weekday6_to,0,5)}" type="text"></td>
						<td><input name="weekday7_to" id="weekday7_to" maxlength="5" size="5" value="{substr($location->weekday7_to,0,5)}" type="text"></td>
						<td><input name="allwkday_to" id="allwkday_to" maxlength="5" size="5" type="text"></td>
					</tr>
					</tbody></table>
				</div>
				<label>{l s='Latitude:' mod='agilepickupcenter'}</label>
				<div class="margin-form">
					<input name="latitude" id="latitude" value="{$location->latitude}" class="" size="33" type="text">
				</div>
				<label>{l s='Longitude:' mod='agilepickupcenter'}</label>
				<div class="margin-form">
					<input name="longitude" id="longitude" value="{$location->longitude}" class="" size="33" type="text">
				</div>
				<label>{l s='Map:' mod='agilepickupcenter'}</label>
		                <div style="margin:0px 0px 0px 5px;">
		                    {include file="$agilepickupcenter_module./views/templates/googlemap.tpl"}
		            </div>

			</fieldset>
			<center>
			        <input name="submitPickupinfo" id="submitPickupinfo" value="Save" class="button" type="submit">
			</center>
		</form>
		<br/><br/>
	{/if}
{/if}
{include file="$agilemultipleseller_views./templates/front/seller_footer.tpl"}

