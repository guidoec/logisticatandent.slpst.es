{capture name=path}<a href="{$link->getPageLink('my-account', true)}">{l s='My Account' mod='agilepickupcenter'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My Seller Account'  mod='agilepickupcenter'}{/capture}

<h1>{l s='My Seller Account' mod='agilepickupcenter'}</h1>
{include file="$tpl_dir./errors.tpl"}

{include file="$agilemultipleseller_views./templates/front/seller_tabs.tpl"}
{if isset($isSeller) AND $isSeller}
<div class="block-center" id="block-history">
    <p style="float:left;padding:10px;">
        <a class="agile-btn agile-btn-default" href="{$link->getModuleLink('agilepickupcenter', 'pickuplocationdetail', ['id_location' =>0], true)}" class="button">
				<i class="icon-plus-sign"></i> {l s='Add New' mod='agilepickupcenter'}
		</a>
    </p>
    <br />
    {if $locations && count($locations)}
	{include file="$tpl_dir./pagination.tpl"}
    <table id="pickuplocation-list" class="std">
        <thead>
	        <tr>
		        <th class="first_item">{l s='ID' mod='agilepickupcenter'}</th>
		        <th class="item">{l s='Carrier' mod='agilepickupcenter'}</th>
			<th class="item">{l s='Location' mod='agilepickupcenter'}</th>
			<th class="item">{l s='Address' mod='agilepickupcenter'}</th>
			<th class="item">{l s='Postal Code' mod='agilepickupcenter'}</th>
			<th class="item">{l s='City' mod='agilepickupcenter'}</th>
			<th class="item">{l s='Active' mod='agilepickupcenter'}</th>
			<th class="item">{l s='Country' mod='agilepickupcenter'}</th>
			<th class="last_item" style="width:5px;">Actions</th>
	        </tr>
        </thead>
        <tbody>
        {foreach from=$locations item=location name=myLoop}
    	{assign var='detail_url' value=$link->getModuleLink('agilepickupcenter', 'pickuplocationdetail', ['id_location' => $location.id_location], true)}
	        <tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
		        <td class="history_link bold">
			        <a class="color-myaccount" href="{$detail_url}">{$location.id_location}</a>
		        </td>
		        <td><a href="{$detail_url}">{$location.carrier}</a></td>
		        <td><a href="{$detail_url}">{$location.location}</a></td>
		        <td><a href="{$detail_url}">{$location.address1}</a></td>
		        <td><a href="{$detail_url}">{$location.postcode}</a></td>
		        <td><a href="{$detail_url}">{$location.city}</a></td>
			<td align="center" valign="middle">
  			    <a href="{$link->getModuleLink('agilepickupcenter', 'pickuplocations', ['process' => 'toggleStatus', 'id_location'=>$location.id_location], true)}">
			            {if $location.active == 1}
			            <img src="{$content_dir|addslashes}img/admin/enabled.gif" />
			            {else}
			            <img src="{$content_dir|addslashes}img/admin/disabled.gif" />
			            {/if}
			    </a>
		        </td>
		        <td><a href="{$detail_url}">{$location.country}</a></td>
			<td class="history_detail" nowrap>
				{if $location.id_location >0 }
				<a href="{$link->getModuleLink('agilepickupcenter', 'pickuplocations', ['process' => 'delete', 'id_location'=>$location.id_location], true)}" onclick="if (confirm('Delete selected item?')){ return true; }else{ event.stopPropagation(); event.preventDefault();};"><img src="{$content_dir|addslashes}img/admin/delete.gif" /></a>
				{/if}
		        </td>
	        </tr>
        {/foreach}
        </tbody>
    </table>
    <div id="block-location-detail" class="hidden">&nbsp;</div>
    {else}
        <p class="warning">{l s='You do not have any location registered' mod='agilepickupcenter'}</p>
    {/if}
</div>
{/if}
{include file="$agilemultipleseller_views./templates/front/seller_footer.tpl"}
