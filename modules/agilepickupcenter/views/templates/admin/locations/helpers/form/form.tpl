{*
*}
{extends file="helpers/form/form.tpl"}

{block name="label"}
	{if $input.type == 'text_customer'}
		<label>{l s='Linked Carrier' mod='agilepickupcenter'}</label>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}

{block name="field"}
	{if $input.type == 'google_map'}
        {include file="./googlemap.tpl"}
	{elseif $input.type == 'text_carrier'}
		<div class="margin-form">
			{if isset($carrier)}
				<a style="display: block; padding-top: 4px;" href="{$urlEditCarrier}">
				{$carrier->name}
				</a>
			{/if}
			<input type="hidden" name="id_carrier" value="{if isset($carrier)}{$carrier->id}{/if}" />	
		</div>
	{elseif $input.type == 'operation_info'}
		<div class="margin-form">
			<script type="text/javascript">
				$(document).ready(function() {
					$("input#allwkday_on").click(function() {
    					var $radios = $("input:radio[name^='weekday']");
						if ($radios.is(":checked") === false) {
							$radios.filter("[value=0]").attr("checked", true);
						}
						else {
							$radios.filter("[value=1]").attr("checked", true);
						}
					});
					$("input#allwkday_off").click(function() {
						var $radios = $("input:radio[name^='weekday']");
						if ($radios.is(":checked") === true) {
							$radios.filter("[value=0]").attr("checked", true);
						}
						else {
							$radios.filter("[value=1]").attr("checked", true);
						}
    				});
					$("input#allwkday_from").change(function() {
						var val = $("input#allwkday_from").val();
						$("input#weekday1_from").val(val);
						$("input#weekday2_from").val(val);
						$("input#weekday3_from").val(val);
						$("input#weekday4_from").val(val);
						$("input#weekday5_from").val(val);
						$("input#weekday6_from").val(val);
						$("input#weekday7_from").val(val);
					});
					$("input#allwkday_to").change(function() {
						var val = $("input#allwkday_to").val();
						$("input#weekday1_to").val(val);
						$("input#weekday2_to").val(val);
						$("input#weekday3_to").val(val);
						$("input#weekday4_to").val(val);
						$("input#weekday5_to").val(val);
						$("input#weekday6_to").val(val);
						$("input#weekday7_to").val(val);
					});
				});
    
			</script>

		    <table>
		    <thead><tr>
		        <th align="center">{l s='Mon' mod='agilepickupcenter'}</th>
		        <th align="center">{l s='Tue' mod='agilepickupcenter'}</th>
		        <th align="center">{l s='Wed' mod='agilepickupcenter'}</th>
		        <th align="center">{l s='Thu' mod='agilepickupcenter'}</th>
		        <th align="center">{l s='Fri' mod='agilepickupcenter'}</th>
		        <th align="center">{l s='Sat' mod='agilepickupcenter'}</th>
		        <th align="center">{l s='Sun' mod='agilepickupcenter'}</th>
		        <th align="center">{l s='All' mod='agilepickupcenter'}</th>
		     </tr></thead>
		     <tr>
				 <td align="center">
					<input type="radio" name="weekday1" id="weekday1_on" value="1" {if $location_obj->weekday1}checked="checked"{/if} />
	 					<label class="t" for="weekday1_on"><img src="../img/admin/enabled.gif" alt="{l s='Open' mod='agilepickupcenter'}" title="{l s='Open' mod='agilepickupcenter'}" /></label><br />
	   				<input type="radio" name="weekday1" id="weekday1_off" value="0" {if !$location_obj->weekday1}checked="checked"{/if}/>
		    			<label class="t" for="weekday1_off"><img src="../img/admin/disabled.gif" alt="{l s='Close' mod='agilepickupcenter'}" title="{l s='Close' mod='agilepickupcenter'}" /></label>
				</td>
				 <td align="center">
					<input type="radio" name="weekday2" id="weekday2_on" value="1" {if $location_obj->weekday2}checked="checked"{/if} />
	 					<label class="t" for="weekday2_on"><img src="../img/admin/enabled.gif" alt="{l s='Open' mod='agilepickupcenter'}" title="{l s='Open' mod='agilepickupcenter'}" /></label><br />
	   				<input type="radio" name="weekday2" id="weekday2_off" value="0" {if !$location_obj->weekday2}checked="checked"{/if}/>
		    			<label class="t" for="weekday2_off"><img src="../img/admin/disabled.gif" alt="{l s='Close' mod='agilepickupcenter'}" title="{l s='Close' mod='agilepickupcenter'}" /></label>
				</td>
				 <td align="center">
					<input type="radio" name="weekday3" id="weekday3_on" value="1" {if $location_obj->weekday3}checked="checked"{/if} />
	 					<label class="t" for="weekday3_on"><img src="../img/admin/enabled.gif" alt="{l s='Open' mod='agilepickupcenter'}" title="{l s='Open' mod='agilepickupcenter'}" /></label><br />
	   				<input type="radio" name="weekday3" id="weekday3_off" value="0" {if !$location_obj->weekday3}checked="checked"{/if}/>
		    			<label class="t" for="weekday3_off"><img src="../img/admin/disabled.gif" alt="{l s='Close' mod='agilepickupcenter'}" title="{l s='Close' mod='agilepickupcenter'}" /></label>
				</td>
				 <td align="center">
					<input type="radio" name="weekday4" id="weekday4_on" value="1" {if $location_obj->weekday4}checked="checked"{/if} />
	 					<label class="t" for="weekday4_on"><img src="../img/admin/enabled.gif" alt="{l s='Open' mod='agilepickupcenter'}" title="{l s='Open' mod='agilepickupcenter'}" /></label><br />
	   				<input type="radio" name="weekday4" id="weekday4_off" value="0" {if !$location_obj->weekday4}checked="checked"{/if}/>
		    			<label class="t" for="weekday4_off"><img src="../img/admin/disabled.gif" alt="{l s='Close' mod='agilepickupcenter'}" title="{l s='Close' mod='agilepickupcenter'}" /></label>
				</td>
				 <td align="center">
					<input type="radio" name="weekday5" id="weekday5_on" value="1" {if $location_obj->weekday5}checked="checked"{/if} />
	 					<label class="t" for="weekday5_on"><img src="../img/admin/enabled.gif" alt="{l s='Open' mod='agilepickupcenter'}" title="{l s='Open' mod='agilepickupcenter'}" /></label><br />
	   				<input type="radio" name="weekday5" id="weekday5_off" value="0" {if !$location_obj->weekday5}checked="checked"{/if}/>
		    			<label class="t" for="weekday5_off"><img src="../img/admin/disabled.gif" alt="{l s='Close' mod='agilepickupcenter'}" title="{l s='Close' mod='agilepickupcenter'}" /></label>
				</td>
				 <td align="center">
					<input type="radio" name="weekday6" id="weekday6_on" value="1" {if $location_obj->weekday6}checked="checked"{/if} />
	 					<label class="t" for="weekday6_on"><img src="../img/admin/enabled.gif" alt="{l s='Open' mod='agilepickupcenter'}" title="{l s='Open' mod='agilepickupcenter'}" /></label><br />
	   				<input type="radio" name="weekday6" id="weekday6_off" value="0" {if !$location_obj->weekday6}checked="checked"{/if}/>
		    			<label class="t" for="weekday6_off"><img src="../img/admin/disabled.gif" alt="{l s='Close' mod='agilepickupcenter'}" title="{l s='Close' mod='agilepickupcenter'}" /></label>
				</td>
				 <td align="center">
					<input type="radio" name="weekday7" id="weekday7_on" value="1" {if $location_obj->weekday7}checked="checked"{/if} />
	 					<label class="t" for="weekday7_on"><img src="../img/admin/enabled.gif" alt="{l s='Open' mod='agilepickupcenter'}" title="{l s='Open' mod='agilepickupcenter'}" /></label><br />
	   				<input type="radio" name="weekday7" id="weekday7_off" value="0" {if !$location_obj->weekday7}checked="checked"{/if}/>
		    			<label class="t" for="weekday7_off"><img src="../img/admin/disabled.gif" alt="{l s='Close' mod='agilepickupcenter'}" title="{l s='Close' mod='agilepickupcenter'}" /></label>
				</td>
    			 <td align="center">
	    		 <input type="radio" name="allwkday" id="allwkday_on" value="1" />
					 <label class="t" for="allwkday_on"><img src="../img/admin/enabled.gif" alt="{l s='Open' mod='agilepickupcenter'}" title="{l s='Open' mod='agilepickupcenter'}" /></label><br />
				 <input type="radio" name="allwkday" id="allwkday_off" value="0" />
					 <label class="t" for="allwkday_off"><img src="../img/admin/disabled.gif" alt="{l s='Close' mod='agilepickupcenter'}" title="{l s='Close' mod='agilepickupcenter'}" /></label>
				 </td>
		     </tr>
		     <tr>
		     <td><input type="text" name="weekday1_from" id="weekday1_from" maxlength="5" size=5 value="{substr($location_obj->weekday1_from,0,5)}" /></td>
		     <td><input type="text" name="weekday2_from" id="weekday2_from" maxlength="5" size=5 value="{substr($location_obj->weekday2_from,0,5)}" /></td>
		     <td><input type="text" name="weekday3_from" id="weekday3_from" maxlength="5" size=5 value="{substr($location_obj->weekday3_from,0,5)}" /></td>
		     <td><input type="text" name="weekday4_from" id="weekday4_from" maxlength="5" size=5 value="{substr($location_obj->weekday4_from,0,5)}" /></td>
		     <td><input type="text" name="weekday5_from" id="weekday5_from" maxlength="5" size=5 value="{substr($location_obj->weekday5_from,0,5)}" /></td>
		     <td><input type="text" name="weekday6_from" id="weekday6_from" maxlength="5" size=5 value="{substr($location_obj->weekday6_from,0,5)}" /></td>
		     <td><input type="text" name="weekday7_from" id="weekday7_from" maxlength=5 size=5 value="{substr($location_obj->weekday7_from,0,5)}" /></td>
		     <td><input type="text" name="allwkday_from" id="allwkday_from" maxlength=5 size=5 value="" /></td>
		     </tr>
		     <tr>
		     <td><input type="text" name="weekday1_to" id="weekday1_to" maxlength="5" size=5 value="{substr($location_obj->weekday1_to,0,5)}" /></td>
		     <td><input type="text" name="weekday2_to" id="weekday2_to" maxlength="5" size=5 value="{substr($location_obj->weekday2_to,0,5)}" /></td>
		     <td><input type="text" name="weekday3_to" id="weekday3_to" maxlength="5" size=5 value="{substr($location_obj->weekday3_to,0,5)}" /></td>
		     <td><input type="text" name="weekday4_to" id="weekday4_to" maxlength="5" size=5 value="{substr($location_obj->weekday4_to,0,5)}" /></td>
		     <td><input type="text" name="weekday5_to" id="weekday5_to" maxlength="5" size=5 value="{substr($location_obj->weekday5_to,0,5)}" /></td>
		     <td><input type="text" name="weekday6_to" id="weekday6_to" maxlength="5" size=5 value="{substr($location_obj->weekday6_to,0,5)}" /></td>
		     <td><input type="text" name="weekday7_to" id="weekday7_to" maxlength="5" size=5 value="{substr($location_obj->weekday7_to,0,5)}" /></td>
		     <td><input type="text" name="allwkday_to" id="allwkday_to" maxlength=5 size=5 value="" /></td>
		     </tr>
		    </table>

		</div>
	{else}
		{$smarty.block.parent}
	{/if}
{/block}
