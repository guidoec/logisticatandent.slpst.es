<?php

///-build_id: 2014112218.0553

/// This source file is subject to the Software License Agreement that is bundled with this 

/// package in the file license.txt, or you can get it here

/// http://addons-modules.com/en/content/3-terms-and-conditions-of-use

///

/// @copyright  2009-2012 Addons-Modules.com

///  If you need open code to customize or merge code with othe modules, please contact us.

class OrderOpcController extends OrderOpcControllerCore

{

	protected function _getPaymentMethods()

	{

		global $cart;

				if(Module::isInstalled('agilepickupcenter') AND $cart->id_carrier == intval(Configuration::getGlobalValue('AGILE_PICKUPCENTER_CARRIER_ID')))

		{

	        include_once(_PS_ROOT_DIR_  . "/modules/agilepickupcenter/Location.php");

		

	        $cartLocationInfo = Location::getCartLocationInfo($cart->id);

	        $id_location = 0;

	        if(isset($cartLocationInfo) AND intval($cartLocationInfo['id_location']) > 0)$id_location = intval($cartLocationInfo['id_location']);

			if($id_location <= 0)return '<p class="warning">'.Tools::displayError('Please choose pickup location').'</p>';

		}

		

		return parent::_getPaymentMethods();

	}    

}

