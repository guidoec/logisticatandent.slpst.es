<?php /*
///-build_id: 2014112217.3321
/// This source file is subject to the Software License Agreement that is bundled with this 
/// package in the file license.txt, or you can get it here
/// http://addons-modules.com/store/en/content/3-terms-and-conditions-of-use
///
/// @copyright  2009-2012 Addons-Modules.com
///
*/
$ {
"GLOBALS"
}
["tcyqiysdpbn"] = "idx";
$ {
"GLOBALS"
}
["mosxrmbnn"] = "rows";
$ {
"GLOBALS"
}
["gelaeghbtv"] = "id_cart";
$ {
"GLOBALS"
}
["hngsncgmw"] = "distance";
$ {
"GLOBALS"
}
["mwljmdcpm"] = "centerLng";
$ {
"GLOBALS"
}
["ypgflxbhbls"] = "centerLat";
$ {
"GLOBALS"
}
["xocnvmbjbfu"] = "sql";
$ {
"GLOBALS"
}
["xujivrpsrrw"] = "id_carrier";
$ {
"GLOBALS"
}
["rgmclfi"] = "cond_caiirer";
$ {
"GLOBALS"
}
["omwftzozcu"] = "fields";
$ {
"GLOBALS"
}
["xoklgtdf"] = "obj";
$ {
"GLOBALS"
}
["lrblhgenfmg"] = "class";
$ {
"GLOBALS"
}
["lweiwmnttox"] = "id_lang";
$ {
"GLOBALS"
}
["slfwmit"] = "result";
$ {
"GLOBALS"
}
["exbgcrmlnmj"] = "id_address";
class Location extends ObjectModel {
    public $id_carrier;
    public $id_location;
    public $location;
    public $id_country;
    public $country;
    public $id_state;
    public $state;
    public $address1;
    public $address2;
    public $postcode;
    public $city;
    public $description;
    public $phone;
    public $email;
    public $active;
    public $id_seller;
    public $fax;
    public $date_add;
    public $date_upd;
    public $deleted = 0;
    public $latitude;
    public $longitude;
    public $weekday1;
    public $weekday2;
    public $weekday3;
    public $weekday4;
    public $weekday5;
    public $weekday6;
    public $weekday7;
    public $weekday1_from;
    public $weekday2_from;
    public $weekday3_from;
    public $weekday4_from;
    public $weekday5_from;
    public $weekday6_from;
    public $weekday7_from;
    public $weekday1_to;
    public $weekday2_to;
    public $weekday3_to;
    public $weekday4_to;
    public $weekday5_to;
    public $weekday6_to;
    public $weekday7_to;
    private static $_idZones = array();
    private static $_idCountries = array();
    protected $fieldsRequired = array('location', 'id_carrier', 'id_country', 'address1', 'postcode', 'city');
    protected $fieldsSize = array('location' => 128, 'address1' => 128, 'address2' => 128, 'postcode' => 12, 'city' => 64, 'description' => 300, 'phone' => 16, 'fax' => 16);
    protected $fieldsValidate = array('id_carrier' => 'isUnsignedId', 'id_country' => 'isUnsignedId', 'id_state' => 'isNullOrUnsignedId', 'address1' => 'isAddress', 'address2' => 'isAddress', 'postcode' => 'isPostCode', 'city' => 'isCityName', 'description' => 'isMessage', 'phone' => 'isPhoneNumber', 'fax' => 'isPhoneNumber', 'deleted' => 'isBool', 'weekday1' => 'isBool', 'weekday2' => 'isBool', 'weekday3' => 'isBool', 'weekday4' => 'isBool', 'weekday5' => 'isBool', 'weekday6' => 'isBool', 'weekday7' => 'isBool');
    protected $table = 'location';
    protected $identifier = 'id_location';
    protected $_includeContainer = false;
    public function __construct($id_address = NULL, $id_lang = NULL) {
        parent::__construct($ {
        $ {
        "GLOBALS"
        }
        ["exbgcrmlnmj"]
        });
        if ($this->id) {
            $ {
            "GLOBALS"
            }
            ["ylpeygqd"] = "id_lang";
            $ {
            $ {
            "GLOBALS"
            }
            ["slfwmit"]
            } = Db::getInstance()->getRow("SELECT `name` FROM `" . _DB_PREFIX_ . "country_lang`
												WHERE `id_country` = " . intval($this->id_country) . "
												AND `id_lang` = " . ($ {
                $ {
                "GLOBALS"
                }
                ["ylpeygqd"]
                } ? intval($ {
                $ {
                "GLOBALS"
                }
                ["lweiwmnttox"]
                }) : Configuration::get("PS_LANG_DEFAULT")));
            $this->country = $ {
            $ {
            "GLOBALS"
            }
            ["slfwmit"]
            }
            ["name"];
            if (intval($this->id_state) > 0) {
                $ {
                "GLOBALS"
                }
                ["gvtljfvzxn"] = "result";
                $ {
                $ {
                "GLOBALS"
                }
                ["gvtljfvzxn"]
                } = Db::getInstance()->getRow("SELECT `name` FROM `" . _DB_PREFIX_ . "state`
												WHERE `id_state` = " . intval($this->id_state));
                $this->state = $ {
                $ {
                "GLOBALS"
                }
                ["slfwmit"]
                }
                ["name"];
            }
        }
    }
    public function delete() {
        if (!$this->isUsed()) return parent::delete();
        else {
            $ {
            "GLOBALS"
            }
            ["tzuxix"] = "class";
            $ {
            $ {
            "GLOBALS"
            }
            ["lrblhgenfmg"]
            } = get_class($this);
            $ {
            $ {
            "GLOBALS"
            }
            ["xoklgtdf"]
            } = new $ {
            $ {
            "GLOBALS"
            }
            ["tzuxix"]
            }
            ($this->id);
            $obj->deleted = true;
            return $obj->update();
        }
    }
    public function isUsed() {
        return Db::getInstance()->getValue("
		SELECT `id_location`
		FROM `" . _DB_PREFIX_ . "cart_location`
		WHERE `id_location` = " . (int)$this->id);
    }
    public function getFields() {
        $ {
        "GLOBALS"
        }
        ["ojwcrwopbl"] = "fields";
        $ {
        "GLOBALS"
        }
        ["jscietejvxq"] = "fields";
        $ {
        "GLOBALS"
        }
        ["vlufjgg"] = "fields";
        parent::validateFields();
        $ {
        "GLOBALS"
        }
        ["npxegvlgvok"] = "fields";
        if (isset($this->id)) $ {
        $ {
        "GLOBALS"
        }
        ["vlufjgg"]
        }
        ["id_location"] = intval($this->id);
        $ {
        "GLOBALS"
        }
        ["vamffiy"] = "fields";
        $ {
        "GLOBALS"
        }
        ["vrsvwpy"] = "fields";
        $epufnfigce = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["id_carrier"] = intval($this->id_carrier);
        $ {
        $ {
        "GLOBALS"
        }
        ["vamffiy"]
        }
        ["id_country"] = intval($this->id_country);
        $iuwlonqgss = "fields";
        $ {
        "GLOBALS"
        }
        ["ygtkooidb"] = "fields";
        $ {
        "GLOBALS"
        }
        ["xwpqhlsfcd"] = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["id_state"] = intval($this->id_state);
        $ {
        $ {
        "GLOBALS"
        }
        ["ojwcrwopbl"]
        }
        ["location"] = pSQL($this->location);
        $ {
        $iuwlonqgss
        }
        ["address1"] = pSQL($this->address1);
        $ {
        "GLOBALS"
        }
        ["rwdcqayjm"] = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["rwdcqayjm"]
        }
        ["address2"] = pSQL($this->address2);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["postcode"] = pSQL($this->postcode);
        $hhnmlyeiwb = "fields";
        $ {
        "GLOBALS"
        }
        ["uoubbfppwwv"] = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["city"] = pSQL($this->city);
        $ {
        $epufnfigce
        }
        ["description"] = pSQL($this->description);
        $ondqdwqwalv = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["xwpqhlsfcd"]
        }
        ["phone"] = pSQL($this->phone);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["email"] = pSQL($this->email);
        $ {
        $ {
        "GLOBALS"
        }
        ["npxegvlgvok"]
        }
        ["active"] = intval($this->active);
        $ {
        $ {"GLOBALS"}["vrsvwpy"]}["id_seller"] = intval($this->id_seller);
        $ {
        $ {
        "GLOBALS"
        }
        ["jscietejvxq"]
        }
        ["fax"] = pSQL($this->fax);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["deleted"] = intval($this->deleted);
        $elocixrhbcw = "fields";
        $vsuynmxojr = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["date_add"] = pSQL($this->date_add);
        $ {
        "GLOBALS"
        }
        ["tgqwxqdgcpe"] = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["date_upd"] = pSQL($this->date_upd);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["latitude"] = pSQL($this->latitude);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["longitude"] = pSQL($this->longitude);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday1"] = intval($this->weekday1);
        $ {
        $ {
        "GLOBALS"
        }
        ["tgqwxqdgcpe"]
        }
        ["weekday2"] = intval($this->weekday2);
        $ {
        $ondqdwqwalv
        }
        ["weekday3"] = intval($this->weekday3);
        $ {
        $ {
        "GLOBALS"
        }
        ["uoubbfppwwv"]
        }
        ["weekday4"] = intval($this->weekday4);
        $ {
        $ {
        "GLOBALS"
        }
        ["ygtkooidb"]
        }
        ["weekday5"] = intval($this->weekday5);
        $iuangbty = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday6"] = intval($this->weekday6);
        $ {
        "GLOBALS"
        }
        ["nrsvftgmy"] = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday7"] = intval($this->weekday7);
        $ {
        $ {
        "GLOBALS"
        }
        ["nrsvftgmy"]
        }
        ["weekday1_from"] = pSQL($this->weekday1_from);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday2_from"] = pSQL($this->weekday2_from);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday3_from"] = pSQL($this->weekday3_from);
        $bimyeeqbxffk = "fields";
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday4_from"] = pSQL($this->weekday4_from);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday5_from"] = pSQL($this->weekday5_from);
        $ {
        $elocixrhbcw
        }
        ["weekday6_from"] = pSQL($this->weekday6_from);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday7_from"] = pSQL($this->weekday7_from);
        $ {
        $vsuynmxojr
        }
        ["weekday1_to"] = pSQL($this->weekday1_to);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday2_to"] = pSQL($this->weekday2_to);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday3_to"] = pSQL($this->weekday3_to);
        $ {
        $hhnmlyeiwb
        }
        ["weekday4_to"] = pSQL($this->weekday4_to);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday5_to"] = pSQL($this->weekday5_to);
        $ {
        $iuangbty
        }
        ["weekday6_to"] = pSQL($this->weekday6_to);
        $ {
        $ {
        "GLOBALS"
        }
        ["omwftzozcu"]
        }
        ["weekday7_to"] = pSQL($this->weekday7_to);
        return $ {
        $bimyeeqbxffk
        };
    }
    public function getFullAddress($withLocationName = true) {
        $ {
        "GLOBALS"
        }
        ["sfkgeffyf"] = "withLocationName";
        return ($ {
        $ {
        "GLOBALS"
        }
        ["sfkgeffyf"]
        } ? "[" . $this->id_location . "] " . ($this->location . " - ") : "") . $this->address1 . " " . $this->address2 . "," . $this->city . " " . $this->state . " " . $this->postcode. 0;
    }
    public static function getLocations($id_lang, $id_carrier = 0, $centerLat = 0, $centerLng = 0, $distance = 0) {
        $ {
        "GLOBALS"
        }
        ["vmfkfqcpzcet"] = "id_lang";
        $ {
        "GLOBALS"
        }
        ["eropsh"] = "distance";
        $vwwpno = "id_carrier";
        $ {
        $ {
        "GLOBALS"
        }
        ["rgmclfi"]
        } = "";
        if (intval($ {
            $vwwpno
            }) > 0) $ {
        $ {
        "GLOBALS"
        }
        ["rgmclfi"]
        } = " AND id_carrier=" . $ {
            $ {
            "GLOBALS"
            }
            ["xujivrpsrrw"]
            } . " ";
        $ {
        $ {
        "GLOBALS"
        }
        ["xocnvmbjbfu"]
        } = "SELECT a.*
            	,ROUND( SQRT( POW((69.1 * (" . $ {
            $ {
            "GLOBALS"
            }
            ["ypgflxbhbls"]
            } . " - latitude)), 2) + POW((53 * (" . $ {
            $ {
            "GLOBALS"
            }
            ["mwljmdcpm"]
            } . " - longitude)), 2)), 1) AS distance
	            ,LEAST(IFNULL(weekday1_from,'00:01'),IFNULL(weekday2_from,'00:01'),IFNULL(weekday3_from,'00:01'),IFNULL(weekday4_from,'00:01'),IFNULL(weekday5_from,'00:01'),IFNULL(weekday6_from,'00:01'),IFNULL(weekday7_from,'00:01')) AS time_from 
	            ,GREATEST(IFNULL(weekday1_to,'23:59'),IFNULL(weekday2_to,'23:59'),IFNULL(weekday3_to,'23:59'),IFNULL(weekday4_to,'23:59'),IFNULL(weekday5_to,'23:59'),IFNULL(weekday6_to,'23:59'),IFNULL(weekday7_to,'23:59')) AS time_to 
	            ,cl.name AS country FROM `" . _DB_PREFIX_ . "location` a 
	            LEFT JOIN `" . _DB_PREFIX_ . "country_lang` cl ON (a.id_country = cl.id_country AND cl.id_lang =" . $ {
            $ {
            "GLOBALS"
            }
            ["vmfkfqcpzcet"]
            } . ")
	            WHERE active=1 AND deleted=0 " . $ {
            $ {
            "GLOBALS"
            }
            ["rgmclfi"]
            } . "
	            ";
        if ($ {
            $ {
            "GLOBALS"
            }
            ["hngsncgmw"]
            } > 0) $ {
        $ {
        "GLOBALS"
        }
        ["xocnvmbjbfu"]
        } = "SELECT * FROM (" . $ {
            $ {
            "GLOBALS"
            }
            ["xocnvmbjbfu"]
            } . ") AS v WHERE v.distance<= " . $ {
            $ {
            "GLOBALS"
            }
            ["eropsh"]
            };
        $ {
        $ {
        "GLOBALS"
        }
        ["xocnvmbjbfu"]
        } = $ {
            $ {
            "GLOBALS"
            }
            ["xocnvmbjbfu"]
            } . " ORDER BY id_carrier ASC, distance ASC";
        return Db::getInstance()->ExecuteS($ {
        $ {
        "GLOBALS"
        }
        ["xocnvmbjbfu"]
        });
    }
    public static function get_distance($id_location, $centerLat = 0, $centerLng = 0) {
        $ {
        "GLOBALS"
        }
        ["lbegxsygyeom"] = "centerLat";
        $wdanyu = "id_location";
        $ {
        "GLOBALS"
        }
        ["phgfjcqol"] = "sql";
        $ {
        $ {
        "GLOBALS"
        }
        ["phgfjcqol"]
        } = "SELECT ROUND( SQRT( POW((69.1 * (" . $ {
            $ {
            "GLOBALS"
            }
            ["lbegxsygyeom"]
            } . " - latitude)), 2) + POW((53 * (" . $ {
            $ {
            "GLOBALS"
            }
            ["mwljmdcpm"]
            } . " - longitude)), 2)), 1) AS distance
	            FROM `" . _DB_PREFIX_ . "location` 
	            WHERE id_location= " . $ {
            $wdanyu
            } . "
	            ";
        return floatval(Db::getInstance()->getValue($ {
        $ {
        "GLOBALS"
        }
        ["xocnvmbjbfu"]
        }));
    }
    public static function getLocationByCart($id_cart) {
        $ {
        "GLOBALS"
        }
        ["iwmgogwlhpsh"] = "id_cart";
        $ {
        "GLOBALS"
        }
        ["cuqgjn"] = "id_cart";
        $dfxiazm = "id_cart";
        $dfqhnya = "sql";
        if (!isset($ {
            $ {
            "GLOBALS"
            }
            ["cuqgjn"]
            }) OR intval($ {
            $ {
            "GLOBALS"
            }
            ["iwmgogwlhpsh"]
            }) <= 0) return 0;
        $ {
        $ {
        "GLOBALS"
        }
        ["xocnvmbjbfu"]
        } = "SELECT l.* FROM `" . _DB_PREFIX_ . "cart_location` cl 
                LEFT JOIN `" . _DB_PREFIX_ . "location` l ON cl.id_location=l.id_location
             WHERE id_cart=" . $ {
            $dfxiazm
            };
        return Db::getInstance()->getRow($ {
        $dfqhnya
        });
    }
    public static function getCartLocationInfo($id_cart) {
        $dpyxgqdgwe = "id_cart";
        $ {
        "GLOBALS"
        }
        ["chcfhkelnyl"] = "id_cart";
        if (!isset($ {
            $ {
            "GLOBALS"
            }
            ["chcfhkelnyl"]
            }) OR intval($ {
            $ {
            "GLOBALS"
            }
            ["gelaeghbtv"]
            }) <= 0) return 0;
        $ {
        $ {
        "GLOBALS"
        }
        ["xocnvmbjbfu"]
        } = "SELECT * FROM `" . _DB_PREFIX_ . "cart_location` WHERE id_cart=" . $ {
            $dpyxgqdgwe
            };
        return Db::getInstance()->getRow($ {
        $ {
        "GLOBALS"
        }
        ["xocnvmbjbfu"]
        });
    }
    public static function getCarrierWithLocation($id_carrier, $id_lang) {
        $ {
        "GLOBALS"
        }
        ["mxfocurb"] = "rows";
        $slvkesweeng = "id_lang";
        $ {
        "GLOBALS"
        }
        ["tvirfjlpa"] = "rows";
        $ {
        "GLOBALS"
        }
        ["lkwbkjotisi"] = "sql";
        $ {
        $ {
        "GLOBALS"
        }
        ["lkwbkjotisi"]
        } = "SELECT a.*, cl.delay  
                FROM  `" . _DB_PREFIX_ . "carrier` a
                    LEFT JOIN `" . _DB_PREFIX_ . "carrier_lang` cl ON (a.id_carrier=cl.id_carrier AND cl.id_lang=" . $ {
            $slvkesweeng
            } . ")
                WHERE a.deleted = 0 and a.active=1 and a.id_carrier=" . $ {
            $ {
            "GLOBALS"
            }
            ["xujivrpsrrw"]
            } . "
        ";
        $ {
        $ {
        "GLOBALS"
        }
        ["mosxrmbnn"]
        } = Db::getInstance()->ExecuteS($ {
        $ {
        "GLOBALS"
        }
        ["xocnvmbjbfu"]
        });
        if (isset($ {
            $ {
            "GLOBALS"
            }
            ["mosxrmbnn"]
            }) AND count($ {
            $ {
            "GLOBALS"
            }
            ["tvirfjlpa"]
            }) > 0) {
            $qzwyzgaxvn = "rows";
            $ {
            "GLOBALS"
            }
            ["vvpqnwm"] = "idx";
            $ {
            "GLOBALS"
            }
            ["qpwbtxvsw"] = "idx";
            for ($ {
                 $ {
                 "GLOBALS"
                 }
                 ["tcyqiysdpbn"]
                 } = 0;$ {
                 $ {
                 "GLOBALS"
                 }
                 ["qpwbtxvsw"]
                 } < count($ {
            $qzwyzgaxvn
            });$ {
                 $ {
                 "GLOBALS"
                 }
                 ["vvpqnwm"]
                 }
                 ++) {
                $ojlinuusv = "idx";
                $ {
                "GLOBALS"
                }
                ["ocfpruervpil"] = "rows";
                $ {
                "GLOBALS"
                }
                ["cnrudydiim"] = "idx";
                $btqlpdekjz = "rows";
                $ {
                $ {
                "GLOBALS"
                }
                ["mosxrmbnn"]
                }
                [$ {
                $ {
                "GLOBALS"
                }
                ["tcyqiysdpbn"]
                }
                ]["img"] = file_exists(_PS_SHIP_IMG_DIR_ . intval($ {
                    $ {
                    "GLOBALS"
                    }
                    ["ocfpruervpil"]
                    }
                    [$ {
                    $ojlinuusv
                    }
                    ]["id_carrier"]) . ".jpg") ? _THEME_SHIP_DIR_ . intval($ {
                    $btqlpdekjz
                    }
                    [$ {
                    $ {
                    "GLOBALS"
                    }
                    ["cnrudydiim"]
                    }
                    ]["id_carrier"]) . ".jpg" : "";
            }
        }
        return array_shift($ {
        $ {
        "GLOBALS"
        }
        ["mxfocurb"]
        });
    }
}
?>