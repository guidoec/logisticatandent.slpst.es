<script type="text/javascript">
    var shipping_address = "{$shipping_address}";    
    var msg_please_select_location = '{$please_select_location}';
    var base_url = '{$base_dir_ssl}';
    var sl_id_carrier = {$sl_id_carrier};
    var sl_id_location = {$sl_id_location};
    var agilecarrier_id = {$agilecarrier.id_carrier};
    var centerLat = 0;
    var centerLng = 0;
    var latlngLocations = [];
    var indexLocations = [];

    var geocoder = new google.maps.Geocoder();
    var map;
    var markersArray = [];
    var infowindow = new google.maps.InfoWindow({literal}{maxWidth:450}{/literal});

     function resetMap() {
         initializeMap($("input#latitude").val(), $("input#longitude").val(), 12, "map_canvas");
         loc = new google.maps.LatLng($("input#latitude").val(), $("input#longitude").val());
         addMarker("0", loc);
     }

	$(document).ready(function () {
		$('form#form').submit(function () {
			/** _agile_    alert("agilecarrier_id" + agilecarrier_id + " sl_id_carrier:" + sl_id_carrier + " sl_id_location" + sl_id_location); _agile_ **/
			if (agilecarrier_id != sl_id_carrier || sl_id_location > 0) return true;
			alert(msg_please_select_location);
			return false;
		});

		/** _agile_ This block for PrestaShop 1.5x _agile_ **/
		$(".delivery_option_radio").click(function () {
			sl_id_carrier = $(this).val().replace(",","");
			toggleCarriers();
			resetMarkers();
			showMarkerDetail(sl_id_location);
		});


		/** _agile_ This block for PrestaShop 1.4x _agile_ **/
		$("input[id^='id_carrier']").click(function() {
			sl_id_carrier = $(this).val();
			toggleCarriers();
			resetMarkers();
			showMarkerDetail(sl_id_location);
		});

		geocoder.geocode({ "address": shipping_address }, function (results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				centerLat = results[0].geometry.location.lat();
				centerLng = results[0].geometry.location.lng();

				get_location_list(15); //default 15M
				doInitalizationMap();
			} else {
				alert("Geocode was not successful for the following reason: " + status);
			}
		});

});

</script>

    <div id="agilepickupcenter">
    </div>
<div id="map_canvas" class="agile-map-canvas"></div> 
<!-- /Block tags module -->
