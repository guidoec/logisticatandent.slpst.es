
function doInitalizationMap() {
    initializeMap(centerLat, centerLng, 9, "map_canvas");
    window.setTimeout(initToggle, 500);
}

function initToggle() {
    toggleCarriers();
    resetMarkers();
}

function toggleCarriers() {
    if (agilecarrier_id == sl_id_carrier || sl_id_carrier == 0) {
        $('input#id_carrier' + agilecarrier_id).attr("checked", "checked");
        $('div#agilepickupcenter').show();
        $('div#map_canvas').show();
    }
    else {
        $('input#id_carrier' + agilecarrier_id).removeAttr("checked");
        $('div#agilepickupcenter').hide();
        $('div#map_canvas').hide();
    }
}

function resetMarkers() {
    deleteOverlays(); /** _agile_ this is defined in googlemaps.js _agile_  **/
    for (var key in latlngLocations) {
        if ($('tr#tr_location_' + key).is(':visible')) {
            addMarker(key, latlngLocations[key]);
        }
    }
    addMarker("0", new google.maps.LatLng(centerLat, centerLng));
}

function inArray(haystack, needle) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}

function get_location_list(distance_range) {
    var data = "id_carrier" + agilecarrier_id + "&id_location=" + sl_id_location + "&centerLat=" + centerLat + "&centerLng=" + centerLng + "&distance_range=" + distance_range;
    $.ajax({
        type: 'POST',
        url: baseDir + 'modules/agilepickupcenter/ajax_location_list.php',
        data: data,
        success: function (content) {
            $('div#agilepickupcenter').html(content);
            get_location_desc(distance_range);
        }
    });

}


function get_location_desc(distance_range) {
    if (sl_id_location <= 0) return;
    var data = "id_carrier" + agilecarrier_id + "&id_location=" + sl_id_location + "&centerLat=" + centerLat + "&centerLng=" + centerLng + "&distance_range=" + distance_range;
    /** _agile_ alert(data); _agile_  **/
    $.ajax({
        type: 'POST',
        url: baseDir + 'modules/agilepickupcenter/ajax_location_desc.php',
        data: data,
        success: function (content) {
            $('#id_loc_message').html(content);
        }
    });

}
