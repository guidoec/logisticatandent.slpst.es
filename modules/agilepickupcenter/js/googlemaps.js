
function initializeMap(lat, lng, zoom, canvas) {
    /*alert("Initialize" + "lat :" + lat + " lng:" + lng +  " soom:" + zoom + "can:" + canvas);*/
    var centerPos = new google.maps.LatLng(lat, lng);
    var mapOptions = {
        zoom: zoom,
        center: centerPos,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById(canvas), mapOptions);

    google.maps.event.addDomListener(window, 'resize', function () {
        map.setCenter(centerPos);
        /*resetMap();*/
    });
}

function showMarkerDetail(title) {
    if (title == 0) return;
    $.ajax({
        type: 'POST',
        url: baseDir + 'modules/agilepickupcenter/ajax_location_info.php',
        data: 'id_location=' + title,
        success: function (content) {
            marker = getMarkerByTitle(title);
            infowindow.setContent(content);
            infowindow.open(map, marker);
        }
    });

}

function getMarkerByTitle(title) {
    for (idx = 0; idx < markersArray.length; idx++) {
        if (markersArray[idx].title == title) return markersArray[idx];
    }
    return null;
}

function addMarker_old(key, location) {
    marker = new google.maps.Marker({
        position: location,
        map: map,
        title: key
    });
    if (key > 0) {
        var iconname = indexLocations[key] > 20 ? 0 : indexLocations[key];
        var iconimage = new google.maps.MarkerImage(base_url + "modules/agilepickupcenter/icons/" + iconname + ".gif");
        marker.setIcon(iconimage);
    }
    google.maps.event.addListener(marker, 'click', function () {
        showMarkerDetail(key);
    });

    markersArray.push(marker);
}

function addMarker(key,location){
    marker=new google.maps.Marker({
        position:location,
        map:map,
        title:key
    });
    if(key>0){
        var iconname = indexLocations[key] > 20 ? 0 : indexLocations[key];
        var iconimage=new google.maps.MarkerImage(base_url + "modules/agilepickupcenter/icons/" + iconname + ".gif");
        marker.setIcon(iconimage);}
    google.maps.event.addListener(marker,'click',function(){showMarkerDetail(key);});
    markersArray.push(marker);

    var bounds=new google.maps.LatLngBounds();
    for(var i=0;i<markersArray.length;i++){
        bounds.extend(markersArray[i].getPosition());
    }
    map.fitBounds(bounds);
}

/** _agile_  Removes the overlays from the map, but keeps them in the array _agile_  **/
function clearOverlays() {
    if (markersArray) {
        for (i = 0; i < markersArray.length; i++) {
            markersArray[i].setMap(null);
        }
    }
}

/** _agile_  Shows any overlays currently in the array _agile_  **/
function showOverlays() {
    if (markersArray) {
        for (i = 0; i < markersArray.length; i++) {
            markersArray[i].setMap(map);
        }
    }
}

/** _agile_  Deletes all markers in the array by removing references to them _agile_  **/
function deleteOverlays() {
    if (markersArray) {
        for (i = 0; i < markersArray.length; i++) {
            markersArray[i].setMap(null);
        }
        markersArray.length = 0;
    }
}

