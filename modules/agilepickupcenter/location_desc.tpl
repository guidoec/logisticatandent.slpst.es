<div style="height:35px;">
<font color="blue">{l s='The location you selected is' mod='agilepickupcenter'}: <strong style="text-decoration: underline ">{$location->id} - {$location->location}, {$location->city} - ({$loc_distance} {l s='KM' mod='agilepickupcenter'})</strong></font>
<br />
{if $out_of_range==1}<font style="font-weight: bold" color="red">{l s='Note:It is out of your search range(not in the list, not displayed on map)' mod='agilepickupcenter'}</font>{/if}
</div>
