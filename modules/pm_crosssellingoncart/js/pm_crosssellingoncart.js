var pm_csocLoopInterval;
if (typeof($csocjqPm) == 'undefined') $csocjqPm = $;
function pm_reloadCartOnAdd(orderPageURL) {
	if ($('#order-detail-content:first').size() > 0) {
		$('div#csoc-container .ajax_add_to_cart_button').unbind('click');
		$(document).off('click', '.ajax_add_to_cart_button').off('click', 'div#csoc-container .ajax_add_to_cart_button').on('click', 'div#csoc-container .ajax_add_to_cart_button', function(e) {
			e.preventDefault();
			var idProduct =  $(this).data('id-product');
			if (typeof(idProduct) == 'undefined')
				var idProduct =  $(this).attr('rel').replace('nofollow', '').replace('ajax_id_product_', '');
			if ($(this).attr('disabled') != 'disabled') {
				ajaxCart.add(idProduct, null, false, this);
				$('#order-detail-content:first').load(orderPageURL + '?content_only=1 #order-detail-content:first');
				if (typeof(deleteProductFromSummary) !== 'undefined')
					deleteProductFromSummary('0_0');
				if (typeof(getCarrierListAndUpdate) !== 'undefined')
					getCarrierListAndUpdate();
				if (typeof(updatePaymentMethodsDisplay) !== 'undefined')
					updatePaymentMethodsDisplay();
			}
			return false;
		});
	}
}