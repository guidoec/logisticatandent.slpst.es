{if count($csoc_product_selection) > 0}

<div id="csoc-container" class="{$csoc_prefix|strtolower}">
	{if $csoc_bloc_title}<h2 class="page-subheading">{$csoc_bloc_title|escape:'html':'UTF-8'}</h2>{/if}

	<div id="{$csoc_prefix}" class="clearfix">
		{foreach from=$csoc_product_selection item='cartProduct' name=cartProduct}
		<div class="product-container">
			<div class="left-block" style="width: {getWidthSize type=$imageSize}px">
				<div class="product-image-container">
					<a class="product_img_link" href="{$link->getProductLink($cartProduct.id_product, $cartProduct.link_rewrite, $cartProduct.category)}" title="{$cartProduct.name|escape:'html':'UTF-8'}">
						{if empty($cartProduct.link_rewrite)}
							<img src="{$link->getImageLink("default", $cartProduct.id_image, $imageSize)}" alt="{$cartProduct.name|escape:'html':'UTF-8'}" />
						{else}
							<img src="{$link->getImageLink($cartProduct.link_rewrite, $cartProduct.id_image, $imageSize)}" alt="{$cartProduct.name|escape:'html':'UTF-8'}" />
						{/if}
						{if isset($cartProduct.new) && $cartProduct.new == 1}
						<span class="new-box">
							<span class="new-label">{l s='New' mod='pm_crosssellingoncart'}</span>
						</span>
						{/if}
						{if isset($cartProduct.on_sale) && $cartProduct.on_sale && isset($cartProduct.show_price) && $cartProduct.show_price && !$PS_CATALOG_MODE}
						<span class="sale-box">
							<span class="sale-label">{l s='Sale!' mod='pm_crosssellingoncart'}</span>
						</span>
						{/if}
					</a>
				</div><!-- .product-image-container -->
			</div><!-- .left-block -->
			<div class="right-block">
				<h5>
					{if isset($cartProduct.pack_quantity) && $cartProduct.pack_quantity}{$cartProduct.pack_quantity|intval|cat:' x '}{/if}
					<a class="product-name" href="{$cartProduct.link|escape:'html':'UTF-8'}" title="{$cartProduct.name|escape:'html':'UTF-8'}">
						{$cartProduct.name|truncate:45:'...'|escape:'html':'UTF-8'}
					</a>
				</h5>
				{if (!$PS_CATALOG_MODE && $PS_STOCK_MANAGEMENT && ((isset($cartProduct.show_price) && $cartProduct.show_price) || (isset($cartProduct.available_for_order) && $cartProduct.available_for_order)))}
				<div class="content_price">
					{if isset($cartProduct.show_price) && $cartProduct.show_price && !isset($restricted_country_mode)}
						<span class="price product-price">
							{if !$priceDisplay}{convertPrice price=$cartProduct.price}{else}{convertPrice price=$cartProduct.price_tax_exc}{/if}
						</span>
						{if isset($cartProduct.specific_prices) && $cartProduct.specific_prices && isset($cartProduct.specific_prices.reduction) && $cartProduct.specific_prices.reduction}
							<span class="old-price product-price">
								{displayWtPrice p=$cartProduct.price_without_reduction}
							</span>
							{if $cartProduct.specific_prices.reduction_type == 'percentage'}
								<span class="price-percent-reduction">-{$cartProduct.specific_prices.reduction * 100}%</span>
							{/if}
						{/if}
					{else}
						<span class="price product-price">&nbsp;</span>
					{/if}
				</div>
				{/if}
				<div class="button-container">
					{if ($cartProduct.id_product_attribute == 0 || (isset($add_prod_display) && ($add_prod_display == 1))) && $cartProduct.available_for_order && !isset($restricted_country_mode) && $cartProduct.minimal_quantity <= 1 && $cartProduct.customizable != 2 && !$PS_CATALOG_MODE}
						{if ($cartProduct.allow_oosp || $cartProduct.quantity > 0)}
							{if isset($static_token)}
								<a class="button ajax_add_to_cart_button btn btn-default{if $csoc_prefix == 'PM_MC_CSOC'} button-small{/if}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$cartProduct.id_product|intval}&amp;id_product_attribute={$cartProduct.id_product_attribute|intval}&amp;token={$static_token}", false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='pm_crosssellingoncart'}" data-id-product="{$cartProduct.id_product|intval}" data-id-product-attribute="{$cartProduct.id_product_attribute|intval}">
									<span>{l s='Add to cart' mod='pm_crosssellingoncart'}</span>
								</a>
							{else}
								<a class="button ajax_add_to_cart_button btn btn-default{if $csoc_prefix == 'PM_MC_CSOC'} button-small{/if}" href="{$link->getPageLink('cart',false, NULL, "add=1&amp;id_product={$cartProduct.id_product|intval}&amp;id_product_attribute={$cartProduct.id_product_attribute|intval}", false)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Add to cart' mod='pm_crosssellingoncart'}" data-id-product="{$cartProduct.id_product|intval}" data-id-product-attribute="{$cartProduct.id_product_attribute|intval}">
									<span>{l s='Add to cart' mod='pm_crosssellingoncart'}</span>
								</a>
							{/if}
						{else}
							<span class="button ajax_add_to_cart_button btn btn-default{if $csoc_prefix == 'PM_MC_CSOC'} button-small{/if} disabled">
								<span>{l s='Add to cart' mod='pm_crosssellingoncart'}</span>
							</span>
						{/if}
					{/if}
				</div>
				{*
				<div class="product-flags">
					{if (!$PS_CATALOG_MODE AND ((isset($cartProduct.show_price) && $cartProduct.show_price) || (isset($cartProduct.available_for_order) && $cartProduct.available_for_order)))}
						{if isset($cartProduct.online_only) && $cartProduct.online_only}
							<span class="online_only">{l s='Online only' mod='pm_crosssellingoncart'}</span>
						{/if}
					{/if}
					{if isset($cartProduct.on_sale) && $cartProduct.on_sale && isset($cartProduct.show_price) && $cartProduct.show_price && !$PS_CATALOG_MODE}
						{elseif isset($cartProduct.reduction) && $cartProduct.reduction && isset($cartProduct.show_price) && $cartProduct.show_price && !$PS_CATALOG_MODE}
							<span class="discount">{l s='Reduced price!' mod='pm_crosssellingoncart'}</span>
						{/if}
				</div>
				*}
				{if (!$PS_CATALOG_MODE AND ((isset($cartProduct.show_price) && $cartProduct.show_price) || (isset($cartProduct.available_for_order) && $cartProduct.available_for_order)))}
					{if isset($cartProduct.available_for_order) && $cartProduct.available_for_order && !isset($restricted_country_mode)}
						<span class="availability">
							{if ($cartProduct.allow_oosp || $cartProduct.quantity > 0)}
								<span class="available-now">
									{l s='In Stock' mod='pm_crosssellingoncart'}
								</span>
							{elseif (isset($cartProduct.quantity_all_versions) && $cartProduct.quantity_all_versions > 0)}
								<span class="available-dif">
									{l s='Product available with different options' mod='pm_crosssellingoncart'}
								</span>
							{else}
								<span class="out-of-stock">
									{l s='Out of stock' mod='pm_crosssellingoncart'}
								</span>
							{/if}
						</span>
					{/if}
				{/if}
			</div><!-- .right-block -->
		</div><!-- .product-container -->
		{/foreach}
	</div>
</div>

<script>
	if (typeof($csocjqPm) == 'undefined') $csocjqPm = $;
	$csocjqPm(document).ready(function() {
		$csocjqPm("#{$csoc_prefix}").owlCarousel({
			items : {if sizeof($csoc_product_selection) < $csoc_products_quantity}{$csoc_product_selection|@sizeof}{else}{$csoc_products_quantity}{/if},
			slideSpeed : 200,
			paginationSpeed : 800,
			autoPlay : true,
			stopOnHover : true,
			goToFirstSpeed : 1000,
			navigation : false,
			navigationText : ["prev","next"],
			scrollPerPage : true,
			pagination : true,
			baseClass : "owl-carousel",
			theme : "owl-theme",
			mouseDraggable : false,
			responsiveBaseWidth: {if $csoc_prefix == 'PM_CSOC'}window{else}{literal}$csocjqPm('.nyroModalCont'){/literal}{/if}
		});
		if (typeof(modalAjaxCart) == 'undefined' && typeof(ajaxCart) != 'undefined' && typeof(pm_reloadCartOnAdd) != 'undefined' && typeof(pm_csocLoopInterval) == 'undefined') {
			pm_csocLoopInterval = setInterval(function() {
				pm_reloadCartOnAdd('{$csoc_order_page_link}');
			}, 500);
		}
	});
</script>
{/if}