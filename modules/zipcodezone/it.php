<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{zipcodezone}default-bootstrap>addons_a815a0d25160092762d1d5871896891d'] = 'Discovere novi moduli';
$_MODULE['<{zipcodezone}default-bootstrap>addons_0a8e18cfc3c8ac39712415a1d60b6435'] = 'Tariffazioni di spedizione differenti ai codici postali';
$_MODULE['<{zipcodezone}default-bootstrap>addons_eeb2b6fd7000437866249be2bd118db4'] = 'Applicare tariffazioni di spedizione differenti ai codici postali ai propri clienti. ';
$_MODULE['<{zipcodezone}default-bootstrap>addons_ef43de90249995e50103aecc0fbdaa37'] = 'Visualizzare su Addons';
$_MODULE['<{zipcodezone}default-bootstrap>addons_25909502719d2f174c3e63ad4c9c9631'] = 'Freelivery : Consegna gratuita';
$_MODULE['<{zipcodezone}default-bootstrap>addons_a59a9ede083557113205a3e840721557'] = 'Gestire consegna gratuita per vettore i zone per rata di peso o di prezzo Si vuole.';
$_MODULE['<{zipcodezone}default-bootstrap>addons_30b7fe9f129c374cf417a7bf8d2e9084'] = 'Date di consegna avanzate';
$_MODULE['<{zipcodezone}default-bootstrap>addons_dfbd86f5d814066379a3a827fb10b721'] = 'Mostrare date di consegna valutate quando si sceglie un vettore nel carrello.';
$_MODULE['<{zipcodezone}default-bootstrap>addons_57598965d19c80b344f96bef4fc3cb40'] = 'Pagamenti in base all\'importo';
$_MODULE['<{zipcodezone}default-bootstrap>addons_884981c918688f10ffa8634282ef3ccb'] = 'Limitare pagamenti metodi per un totale di ordine massimo e/o minimo';
