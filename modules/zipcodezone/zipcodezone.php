<?php

class ZipCodeZone extends Module
{
	public function __construct()
	{
		$this->name = 'zipcodezone';
		$this->tab = 'shipping_logistics';
		$this->version = '1.2.2';
		$this->module_key = '6c7a3d34fa01934e71f53aae16f2698b';
		$this->author = 'MARICHAL Emmanuel';

		parent::__construct ();

		$this->displayName = $this->l('Shipping fees based on zipcodes');
		$this->description = $this->l('Assign zip codes to zones easily');

		$this->need_instance = false;
		$this->bootstrap = true;

		$this->id_lang = (int)$this->context->language->id;
		$this->iso_lang = Language::getIsoById($this->id_lang);

		$this->addons_id = 5711;

		$this->table_name = 'zip_code_zone';
	}

	public function install()
	{
		@unlink(_PS_CACHE_DIR_.'class_index.php');

		if (file_exists(_PS_ROOT_DIR_.'/override/classes/Address.php'))
			@rename(_PS_OVERRIDE_DIR_.'classes/Address.php', _PS_OVERRIDE_DIR_.'classes/'.mktime().'_ZipCodeZone_Address.php');

		return $this->installDb() && copy(dirname(__FILE__).'/Address.php', _PS_OVERRIDE_DIR_.'classes/Address.php') && parent::install();
	}

	private function installDb()
	{
		return Db::getInstance()->Execute('
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.pSQL($this->table_name).'` (
					`id` int(10) NOT NULL AUTO_INCREMENT,
					`id_country` int(10) DEFAULT NULL,
					`id_zone` int(10) DEFAULT NULL,
					`min` int(10) DEFAULT NULL,
					`max` int(10) DEFAULT NULL,
					PRIMARY KEY (`id`),
					KEY `ZCZCountryIndex` (`id_country`),
					KEY `ZCZZoneIndex` (`id_zone`)
				) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;');
	}

	public function uninstall()
	{
		@rename(_PS_OVERRIDE_DIR_.'classes/Address.php', _PS_OVERRIDE_DIR_.'classes/'.mktime().'_ZipCodeZone_Address.php');
		return parent::uninstall();
	}

	private function addCondition($id_country, $id_zone, $min, $max)
	{
		$country = new Country($id_country);
		if (Validate::isLoadedObject($country) && $country->id_zone == $id_zone)
			return $this->displayError($this->l('You don\'t need to create a condition with the same zone as the country\'s default zone. You can change the default zone for this country in Localization > Countries'));

		if (Db::getInstance()->execute('
			INSERT INTO '._DB_PREFIX_.'zip_code_zone (id_country, id_zone, min, max)
			VALUES('.(int)$id_country.', '.(int)$id_zone.', '.(int)$min.', '.(int)$max.')'))
			return $this->displayConfirmation($this->l('Condition added with success. Remember to update your carrier(s) in Shipping > Carriers to set your shipping fees.'));
		else
			return $this->displayError($this->l('An error occurred'));
	}

	private function updateCondition($id_condition, $id_country, $id_zone, $min, $max)
	{
		$country = new Country($id_country);
		if (Validate::isLoadedObject($country) && $country->id_zone == $id_zone)
			return $this->displayError($this->l('You don\'t need to create a condition with the same zone as the country\'s default zone. You can change the default zone for this country in Localization > Countries'));

		if (Db::getInstance()->execute('
			UPDATE '._DB_PREFIX_.'zip_code_zone
			SET id_country = '.(int)$id_country.', id_zone = '.(int)$id_zone.', min = '.(int)$min.', max = '.(int)$max.'
			WHERE id = '.(int)$id_condition))
			return $this->displayConfirmation($this->l('Condition updated with success. Remember to update your carrier(s) in Shipping > Carriers to set your shipping fees.'));
		else
			return $this->displayError($this->l('An error occurred'));
	}

	private function delCondition($id)
	{
		if (Db::getInstance()->execute('DELETE FROM '._DB_PREFIX_.'zip_code_zone WHERE id = '.(int)$id))
			return $this->displayConfirmation($this->l('Condition deleted with success'));
		else
			return $this->displayError($this->l('An error occurred'));
	}

	private function addCSV($skip = false, $separator = ';')
	{
		ini_set('auto_detect_line_endings', true);

		$lines = file($_FILES['csv']['tmp_name']);
		if ($lines === FALSE)
			return $this->displayError($this->l('An error occurred'));

		$values = array();

		foreach ($lines as $line_num => $line)
			if (!($skip && $line_num == 0))
				$values[] = explode($separator, $line);

		$sql = '';
		foreach ($values as $line)
		{
			if ($sql == '')
				$sql = 'INSERT INTO '._DB_PREFIX_.'zip_code_zone (id_country, id_zone, min, max) VALUES';
			else
				$sql .= ',';
			$sql .= '('.implode(',', array_map('intval', $line)).')';
		}

		if (Db::getInstance()->execute($sql))
			return $this->displayConfirmation($this->l('Conditions added with success'));
		else
			return $this->displayError($this->l('An error occurred'));
	}

	public function createTemplate($tpl_name)
	{
		return $this->context->smarty->createTemplate($this->context->smarty->getTemplateDir(0).$tpl_name, $this->context->smarty);
	}

	public function getContent()
	{
		$this->context->controller->addJS($this->_path.'js/zipcodezone.js');
		$this->context->controller->addCSS($this->_path.'css/modules.css');

		$html = '';

		if (Tools::isSubmit('submitUpdateCondition') || Tools::isSubmit('submitAddCondition'))
		{
			$id_zone = Tools::getValue('id_zone');

			if (!$id_zone && Tools::getValue('zone_name'))
			{
				$zone = new Zone();
				$zone->name = Tools::getValue('zone_name');
				if ($zone->add())
					$id_zone = $zone->id;
				else
					$html .= $this->displayError($this->l('An error occurred while creating the new zone'));
			}

			if ($id_zone)
			{
				if (Tools::getValue('multiple'))
				{
					$min = Tools::getValue('min');
					$max = Tools::getValue('max');
				}
				else
					$min = $max = Tools::getValue('zipcode');

				if ($min > $max)
				{
					$temp = $max;
					$max = $min;
					$min = $temp;
				}

				if (Tools::isSubmit('submitUpdateCondition'))
					$html .= $this->updateCondition(Tools::getValue('id_condition'), Tools::getValue('id_country'), $id_zone, $min, $max);
				else
					$html .= $this->addCondition(Tools::getValue('id_country'), $id_zone, $min, $max);
			}
		}

		if (Tools::isSubmit('delete'.$this->table_name) && (int)Tools::getValue('id'))
			$html .= $this->delCondition(Tools::getValue('id'));

		if (Tools::isSubmit('addCSV') && isset($_FILES['csv']))
			$html .= $this->addCSV(Tools::getIsset('ignore_first'), Tools::getValue('separator'));

		$doc_iso = file_exists(_PS_MODULE_DIR_.$this->name.'/docs/readme_'.$this->iso_lang.'.pdf') ? $this->iso_lang : 'en';
		$this->context->smarty->assign(array(
			'doc_link' => '../modules/'.$this->name.'/docs/readme_'.$doc_iso.'.pdf',
			'add_link' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&newCondition=1&token='.Tools::getAdminTokenLite('AdminModules'),
			'cancel_link' => $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
			'support_link' => 'http://addons.prestashop.com/'.$this->iso_lang.'/contact-community.php?id_product='.$this->id,
			'rating_link' => 'http://addons.prestashop.com/'.$this->iso_lang.'/ratings.php'
		));

		if (Tools::getValue('newCondition'))
			$html .= $this->renderForm();
		else if (Tools::isSubmit('update'.$this->table_name) && Tools::getValue('id'))
			$html .= $this->renderForm(Tools::getValue('id'));
		else
			$html .= $this->renderList();

		$html .= $this->display(__FILE__, 'views/templates/admin/js_assign.tpl');

		return $html;
	}

	private function renderForm($id_condition = false)
	{
		$condition = false;
		if ($id_condition)
		{
			$dbquery = new DbQuery();
			$dbquery->select('id, zcz.id_zone, zcz.id_country, LPAD(zcz.min, LENGTH(zip_code_format), 0) as min, LPAD(zcz.max, LENGTH(zip_code_format), 0) as max');
			$dbquery->from(pSQL($this->table_name), 'zcz');
			$dbquery->leftJoin('country', 'c', 'c.id_country = zcz.id_country');
			$dbquery->where('id = '.(int)$id_condition);
			$condition = Db::getInstance()->getRow($dbquery->build());
		}

		$add_condition = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Add condition'),
					'icon' => 'icon-plus'
				),
				'input' => array(
					array(
						'type' => 'select',
						'label' => $this->l('Country'),
						'name' => 'id_country',
						'desc' => $this->l('Select the country in which you would like to create multiple delivery rates.'),
						'options' => array(
							'query' => Country::getCountries($this->id_lang, true),
							'id' => 'id_country', 'name' => 'name'
						)
					),
					array(
						'type' => 'select',
						'label' => $this->l('Zone'),
						'name' => 'id_zone',
						'desc' => $this->l('Select an existing delivery zone or create a new one (one zone = one delivery rate).'),
						'options' => array(
							'query' => array_merge(Zone::getZones(true), array(array('id_zone' => 0, 'name' => '+ '.$this->l('New zone')))),
							'id' => 'id_zone', 'name' => 'name'
						)
					),
					array(
						'type' => 'text',
						'label' => $this->l('Zone name'),
						'name' => 'zone_name',
						'class' => 'fixed-width-xl'
					),
					array(
						'type' => 'radio',
						'label' => $this->l('Multiple zipcodes'),
						'name' => 'multiple',
						'is_bool' => true,
						'desc' => $this->l('Select yes if you want to choose a range of zipcodes.'),
						'values' => array(
							array(
								'id' => 'yes',
								'value' => 1,
								'label' => $this->l('Yes')
							),
							array(
								'id' => 'no',
								'value' => 0,
								'label' => $this->l('No')
							)
						)
					),
					array(
						'type' => 'text',
						'label' => $this->l('Zip code min'),
						'name' => 'min',
						'class' => 'fixed-width-md'
					),
					array(
						'type' => 'text',
						'label' => $this->l('Zip code max'),
						'name' => 'max',
						'class' => 'fixed-width-md'
					),
					array(
						'type' => 'text',
						'label' => $this->l('Zip code'),
						'name' => 'zipcode',
						'class' => 'fixed-width-md'
					),
				),
				'submit' => array(
					'name' => $condition ? 'updateCondition' : 'addCondition',
					'title' => $condition ? $this->l('Update') : $this->l('Add')
				)
			),
		);

		$csv = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Import CSV'),
					'icon' => 'icon-upload'
				),
				'input' => array(
					array(
						'type' => 'file',
						'label' => $this->l('CSV file'),
						'name' => 'csv',
						'desc' => '<a href="../modules/zipcodezone/example.csv">'.$this->l('Click to see a CSV sample').'</a>'
					),
					array(
						'type' => 'checkbox',
						'label' => $this->l('Ignore first line'),
						'name' => 'ignore',
						'values' => array(
							'query' => array(array('id' => 'first', 'name' => '')),
							'id' => 'id',
							'name' => 'name'
						)
					),
					array(
						'type' => 'text',
						'label' => $this->l('Separator'),
						'name' => 'separator',
						'class' => 'fixed-width-xs'
					),
				),
				'submit' => array(
					'name' => 'addCSV',
					'title' => $this->l('Import')
				)
			),
		);

		$helper = new HelperForm();

		if ($condition)
			$helper->id = $condition['id']; // Hack in JS

		$helper->show_toolbar = false;
		$helper->table = $this->table_name;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$helper->module = $this;
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->submit_action = $condition ? 'submitUpdateCondition' : 'submitAddCondition';

		$fields_value = array(
			'min' => $condition ? $condition['min'] : false,
			'max' => $condition ? $condition['max'] : false,
			'id_zone' => $condition ? $condition['id_zone'] : -1,
			'zipcode' => $condition ? $condition['max'] : false,
			'multiple' => $condition && $condition['min'] != $condition['max'],
			'zone_name' => '',
			'id_country' => $condition ? $condition['id_country'] : 0,
			'csv' => '',
			'first' => '',
			'separator' => ';'
		);

		$helper->tpl_vars = array(
			'fields_value' => $fields_value,
		);

		return $helper->generateForm(array($add_condition)).($condition ? '' : $helper->generateForm(array($csv)));
	}

	private function getConditions()
	{
		$dbquery = new DbQuery();
		$dbquery->select('zcz.id, zcz.id_country, zcz.id_zone, z.name AS zone_name, cl.name AS country_name, IF(zcz.min = zcz.max, LPAD(zcz.min, LENGTH(zip_code_format), 0), CONCAT_WS("-", LPAD(zcz.min, LENGTH(zip_code_format), 0), LPAD(zcz.max, LENGTH(zip_code_format), 0))) as zipcodes');
		$dbquery->from(pSQL($this->table_name), 'zcz');
		$dbquery->leftJoin('zone', 'z', 'z.id_zone = zcz.id_zone');
		$dbquery->leftJoin('country_lang', 'cl', 'cl.id_country = zcz.id_country AND cl.id_lang = '.(int)$this->id_lang);
		$dbquery->leftJoin('country', 'c', 'c.id_country = cl.id_country');
		$dbquery->orderBy('zcz.id_country, zcz.id_zone, zcz.min ASC');
		$dbquery->groupBy('zcz.id');

		return Db::getInstance()->ExecuteS($dbquery->build());
	}

	public function paginateConditions($conditions, $page = 1, $pagination = 50)
	{
		if(count($conditions) > $pagination)
			$conditions = array_slice($conditions, $pagination * ($page - 1), $pagination);

		return $conditions;
	}

	private function renderList()
	{
		$fields_list = array(
			'country_name' => array(
				'title' => $this->l('Country'),
				'type' => 'text',
				'search' => false
			),
			'zipcodes' => array(
				'title' => $this->l('Zip Code(s)'),
				'type' => 'text',
				'search' => false
			),
			'zone_name' => array(
				'title' => $this->l('New Delivery Zone'),
				'type' => 'text',
				'search' => false
			)
		);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->identifier = 'id';
		$helper->actions = array('edit', 'delete');
		$helper->show_toolbar = true;
		$helper->simple_header = false;
		$helper->module = $this;
		$helper->no_link = true;
		$helper->title = $this->l('Conditions');
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->table = $this->table_name;

		$conditions = $this->getConditions();
		$helper->listTotal = count($conditions);

		$page = ($page = Tools::getValue('submitFilter'.$this->table_name)) ? $page : 1;
		$pagination = ($pagination = Tools::getValue($this->table_name.'_pagination')) ? $pagination : 50;
		$conditions = $this->paginateConditions($conditions, $page, $pagination);

		// Adding 'Add' button
		$helper->toolbar_btn['new'] = array(
			'href' => $this->context->link->getAdminLink('AdminModules', false)
			.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name
			.'&newCondition=1&token='.Tools::getAdminTokenLite('AdminModules'),
			'desc' => $this->l('Add new condition')
		);

		// Adding 'Documentation' button
		$doc_iso = file_exists(_PS_MODULE_DIR_.$this->name.'/docs/readme_'.$this->iso_lang.'.pdf') ? $this->iso_lang : 'en';
		$helper->toolbar_btn['help'] = array(
			'href' => '../modules/'.$this->name.'/docs/readme_'.$doc_iso.'.pdf',
			'target' => '_blank',
			'desc' => $this->l('View documentation')
		);

		$html = $helper->generateList($conditions, $fields_list);

		if ($helper->listTotal > 0)
		{
			$this->context->smarty->assign('iso_lang', $this->iso_lang);
			$html .= $this->display(__FILE__, 'views/templates/admin/satisfaction.tpl').$this->display(__FILE__, 'views/templates/admin/addons.tpl');
		}
		return $html;
	}
}