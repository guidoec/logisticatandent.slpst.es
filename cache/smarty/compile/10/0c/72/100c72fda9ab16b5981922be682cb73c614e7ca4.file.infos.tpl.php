<?php /* Smarty version Smarty-3.1.19, created on 2015-04-07 10:31:13
         compiled from "/var/www/proxyfarma.com/themes/default-bootstrap/modules/cheque/views/templates/hook/infos.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5522755655523b1f19e0646-97825187%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '100c72fda9ab16b5981922be682cb73c614e7ca4' => 
    array (
      0 => '/var/www/proxyfarma.com/themes/default-bootstrap/modules/cheque/views/templates/hook/infos.tpl',
      1 => 1428402156,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5522755655523b1f19e0646-97825187',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5523b1f19f8162_99396836',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5523b1f19f8162_99396836')) {function content_5523b1f19f8162_99396836($_smarty_tpl) {?>

<div class="alert alert-info">
<img src="../modules/cheque/cheque.jpg" style="float:left; margin-right:15px;" width="86" height="49">
<p><strong><?php echo smartyTranslate(array('s'=>"This module allows you to accept payments by check.",'mod'=>'cheque'),$_smarty_tpl);?>
</strong></p>
<p><?php echo smartyTranslate(array('s'=>"If the client chooses this payment method, the order status will change to 'Waiting for payment.'",'mod'=>'cheque'),$_smarty_tpl);?>
</p>
<p><?php echo smartyTranslate(array('s'=>"You will need to manually confirm the order as soon as you receive a check.",'mod'=>'cheque'),$_smarty_tpl);?>
</p>
</div>
<?php }} ?>
