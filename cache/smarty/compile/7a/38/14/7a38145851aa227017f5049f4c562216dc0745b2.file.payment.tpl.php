<?php /* Smarty version Smarty-3.1.19, created on 2015-06-19 11:37:57
         compiled from "/var/www/proxyfarma.com/modules/redsys/views/templates/hook/payment.tpl" */ ?>
<?php /*%%SmartyHeaderCode:17780294825583e2f54078f0-36315558%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7a38145851aa227017f5049f4c562216dc0745b2' => 
    array (
      0 => '/var/www/proxyfarma.com/modules/redsys/views/templates/hook/payment.tpl',
      1 => 1432654177,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '17780294825583e2f54078f0-36315558',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'module_dir' => 0,
    'urltpv' => 0,
    'cantidad' => 0,
    'moneda' => 0,
    'pedido' => 0,
    'codigo' => 0,
    'terminal' => 0,
    'trans' => 0,
    'titular' => 0,
    'merchantdata' => 0,
    'nombre' => 0,
    'urltienda' => 0,
    'productos' => 0,
    'UrlOk' => 0,
    'UrlKO' => 0,
    'firma' => 0,
    'idioma_tpv' => 0,
    'tipopago' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5583e2f545bfe1_05548429',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5583e2f545bfe1_05548429')) {function content_5583e2f545bfe1_05548429($_smarty_tpl) {?>

<?php if (@constant('_PS_VERSION_')>=1.6) {?>

<div class="row">
	<div class="col-xs-12 col-md-6">
		<p class="payment_module">
			<a class="bankwire" href="javascript:$('#redsys_form').submit();" title="<?php echo smartyTranslate(array('s'=>'Conectar con el TPV','mod'=>'redsys'),$_smarty_tpl);?>
">	
				<img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
img/tarjetas_redsys.png" alt="<?php echo smartyTranslate(array('s'=>'Conectar con el TPV','mod'=>'redsys'),$_smarty_tpl);?>
" height="48" />
				<?php echo smartyTranslate(array('s'=>'Pagar con tarjeta  - Pasarela de pago Redsys','mod'=>'redsys'),$_smarty_tpl);?>

			</a>
		</p>
	</div>
</div>
<?php } else { ?>
<p class="payment_module">
	<a class="bankwire" href="javascript:$('#redsys_form').submit();" title="<?php echo smartyTranslate(array('s'=>'Conectar con el TPV','mod'=>'redsys'),$_smarty_tpl);?>
">	
		<img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['module_dir']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
img/tarjetas_redsys.png" alt="<?php echo smartyTranslate(array('s'=>'Conectar con el TPV','mod'=>'redsys'),$_smarty_tpl);?>
" height="48" />
		<?php echo smartyTranslate(array('s'=>'Pagar con tarjeta  - Pasarela de pago Redsys','mod'=>'redsys'),$_smarty_tpl);?>

	</a>
</p>
<?php }?>

<form action="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['urltpv']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" method="post" id="redsys_form" class="hidden">	
	<input type="hidden" name="Ds_Merchant_Amount" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['cantidad']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
    <input type="hidden" name="Ds_Merchant_Currency" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['moneda']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_Order" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['pedido']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_MerchantCode" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['codigo']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_Terminal" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['terminal']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_TransactionType" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['trans']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_Titular" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['titular']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_MerchantData" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['merchantdata']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_MerchantName" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['nombre']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_MerchantURL" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['urltienda']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_ProductDescription" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['productos']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_UrlOK" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['UrlOk']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_UrlKO" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['UrlKO']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_MerchantSignature" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['firma']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
	<input type="hidden" name="Ds_Merchant_ConsumerLanguage" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['idioma_tpv']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
    <input type="hidden" name="Ds_Merchant_PayMethods" value="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['tipopago']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
</form><?php }} ?>
