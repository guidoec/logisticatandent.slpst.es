<?php /* Smarty version Smarty-3.1.19, created on 2015-06-19 11:45:49
         compiled from "/var/www/proxyfarma.com/modules/redsys/views/templates/hook/payment_return.tpl" */ ?>
<?php /*%%SmartyHeaderCode:12206124535583e4cdcbbf81-25385333%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0462c24bf337aab27ea22fa4cfd70f5d0df8c7a2' => 
    array (
      0 => '/var/www/proxyfarma.com/modules/redsys/views/templates/hook/payment_return.tpl',
      1 => 1432654177,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '12206124535583e4cdcbbf81-25385333',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'this_path' => 0,
    'status' => 0,
    'shop_name' => 0,
    'total_to_pay' => 0,
    'id_order' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5583e4cdcfdc27_67822077',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5583e4cdcfdc27_67822077')) {function content_5583e4cdcfdc27_67822077($_smarty_tpl) {?>
<img src="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['this_path']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
img/redsys.png" /><br /><br />
<?php if ($_smarty_tpl->tpl_vars['status']->value=='ok') {?>
	<p><?php echo smartyTranslate(array('s'=>'Your order on %s is complete.','sprintf'=>$_smarty_tpl->tpl_vars['shop_name']->value,'mod'=>'redsys'),$_smarty_tpl);?>

		<br /><br />- <?php echo smartyTranslate(array('s'=>'Payment amount.','mod'=>'redsys'),$_smarty_tpl);?>
 <span class="price"><strong><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['total_to_pay']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</strong></span>
		<br /><br />- N# <span class="price"><strong><?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['id_order']->value, ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
</strong></span>
		<br /><br /><?php echo smartyTranslate(array('s'=>'An email has been sent to you with this information.','mod'=>'redsys'),$_smarty_tpl);?>

		<br /><br /><?php echo smartyTranslate(array('s'=>'For any questions or for further information, please contact our','mod'=>'redsys'),$_smarty_tpl);?>
 <a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('contact',true), ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'customer service department.','mod'=>'redsys'),$_smarty_tpl);?>
</a>.
	</p>
<?php } else { ?>
	<p class="warning">
		<?php echo smartyTranslate(array('s'=>'We have noticed that there is a problem with your order. If you think this is an error, you can contact our','mod'=>'redsys'),$_smarty_tpl);?>
 
		<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('contact',true), ENT_QUOTES, 'UTF-8', true);?>
"><?php echo smartyTranslate(array('s'=>'customer service department.','mod'=>'redsys'),$_smarty_tpl);?>
</a>.
	</p>
<?php }?>
<?php }} ?>
