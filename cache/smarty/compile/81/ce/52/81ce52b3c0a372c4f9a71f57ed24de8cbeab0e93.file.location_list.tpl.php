<?php /* Smarty version Smarty-3.1.19, created on 2015-06-25 18:37:28
         compiled from "/var/www/proxyfarma.com/modules/agilepickupcenter/location_list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1670218159558c2e48b4adb5-11023536%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '81ce52b3c0a372c4f9a71f57ed24de8cbeab0e93' => 
    array (
      0 => '/var/www/proxyfarma.com/modules/agilepickupcenter/location_list.tpl',
      1 => 1435219725,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1670218159558c2e48b4adb5-11023536',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'distance_range' => 0,
    'locations' => 0,
    'location' => 0,
    'sl_id_location' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_558c2e48c1f124_34955044',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_558c2e48c1f124_34955044')) {function content_558c2e48c1f124_34955044($_smarty_tpl) {?>    <p id="id_loc_message">
    </p>
    <fieldset>
    <legend style="padding:5px 5px 5px 5px;font-size:16px;"><?php echo smartyTranslate(array('s'=>' pickup locations:','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</legend>
    <p>
    <?php echo smartyTranslate(array('s'=>'Distance scope:','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
 <select id="distance_scope" name="distance_scope" style="width:250px">
        <option value="2" <?php if ($_smarty_tpl->tpl_vars['distance_range']->value==2) {?>selected<?php }?>> <?php echo smartyTranslate(array('s'=>'Less than 2 KM','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</option>
        <option value="5" <?php if ($_smarty_tpl->tpl_vars['distance_range']->value==5) {?>selected<?php }?>> <?php echo smartyTranslate(array('s'=>'Less than 5 KM','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</option>
        <option value="10" <?php if ($_smarty_tpl->tpl_vars['distance_range']->value==10) {?>selected<?php }?>> <?php echo smartyTranslate(array('s'=>'Less than 10 KM','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</option>
        <option value="15" <?php if ($_smarty_tpl->tpl_vars['distance_range']->value==15) {?>selected<?php }?>><?php echo smartyTranslate(array('s'=>'Less than 15 KM','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</option>
        <option value="20" <?php if ($_smarty_tpl->tpl_vars['distance_range']->value==20) {?>selected<?php }?>><?php echo smartyTranslate(array('s'=>'Less than 20 KM','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</option>
        <option value="30" <?php if ($_smarty_tpl->tpl_vars['distance_range']->value==30) {?>selected<?php }?>> <?php echo smartyTranslate(array('s'=>'Less than 30 KM','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</option>
        <option value="50" <?php if ($_smarty_tpl->tpl_vars['distance_range']->value==50) {?>selected<?php }?>> <?php echo smartyTranslate(array('s'=>'Less than 50 KM','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</option>
        <option value="80" <?php if ($_smarty_tpl->tpl_vars['distance_range']->value==80) {?>selected<?php }?>> <?php echo smartyTranslate(array('s'=>'Less than 80 KM','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</option>
        <option value="1000000"><?php echo smartyTranslate(array('s'=>'All locations','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</option>
    </select>
    </p>

    <table width="100%" id="table_agilecarrier" class="std">
        <thead>
        <tr><td><?php echo smartyTranslate(array('s'=>'ID','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</td>
        <td><?php echo smartyTranslate(array('s'=>'Location','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</td>
        <td><?php echo smartyTranslate(array('s'=>'City','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</td>
        <td><?php echo smartyTranslate(array('s'=>'Phone','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</td>
        <td><?php echo smartyTranslate(array('s'=>'Distance','mod'=>'agilepickupcenter'),$_smarty_tpl);?>
</td></tr>
        </thead>
        <tbody>
        <?php  $_smarty_tpl->tpl_vars['location'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['location']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['locations']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['location']->key => $_smarty_tpl->tpl_vars['location']->value) {
$_smarty_tpl->tpl_vars['location']->_loop = true;
?>
            <tr id="tr_location_<?php echo $_smarty_tpl->tpl_vars['location']->value['id_location'];?>
">
            <td><input type="radio" name="id_location" id="id_location_<?php echo $_smarty_tpl->tpl_vars['location']->value['id_location'];?>
" value="<?php echo $_smarty_tpl->tpl_vars['location']->value['id_location'];?>
" <?php if ($_smarty_tpl->tpl_vars['sl_id_location']->value==$_smarty_tpl->tpl_vars['location']->value['id_location']) {?> checked <?php }?>/><?php echo $_smarty_tpl->tpl_vars['location']->value['id_location'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['location']->value['location'];?>
</td><td> <?php echo $_smarty_tpl->tpl_vars['location']->value['city'];?>
</td>
            <td><?php echo $_smarty_tpl->tpl_vars['location']->value['phone'];?>
</td><td id="td_<?php echo $_smarty_tpl->tpl_vars['location']->value['id_location'];?>
" alt="<?php echo $_smarty_tpl->tpl_vars['location']->value['id_location'];?>
"><?php echo $_smarty_tpl->tpl_vars['location']->value['distance'];?>
 KM</td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <div id="location_list">
    </div>
    </fieldset>
            
    <script type="text/javascript">
        <?php  $_smarty_tpl->tpl_vars['location'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['location']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['locations']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['location']->key => $_smarty_tpl->tpl_vars['location']->value) {
$_smarty_tpl->tpl_vars['location']->_loop = true;
?>
            latlngLocations[<?php echo $_smarty_tpl->tpl_vars['location']->value['id_location'];?>
] = new google.maps.LatLng(<?php echo $_smarty_tpl->tpl_vars['location']->value['latitude'];?>
, <?php echo $_smarty_tpl->tpl_vars['location']->value['longitude'];?>
);
            indexLocations[<?php echo $_smarty_tpl->tpl_vars['location']->value['id_location'];?>
] = <?php echo $_smarty_tpl->tpl_vars['location']->value['index'];?>
;
        <?php } ?>
        
        doInitalizationMap();
        
        $("select#distance_scope").change(function() 
        {
            get_location_list($("select#distance_scope").val());            
            get_location_desc($("select#distance_scope").val());
            doInitalizationMap();
        }
        );

        $("input[id^='id_location_']").click(function() 
        {
            sl_id_location = $(this).val();
            showMarkerDetail(sl_id_location);
            get_location_desc($("select#distance_scope").val());
			update_payment_methods();
			
        }
        );

	function update_payment_methods()
	{
		if ($('#cgv:checked').length != 0)
			var checked = 1;
		else
			var checked = 0;

		var data = 'ajax=true&method=updateTOSStatusAndGetPayments&checked=' + checked + '&token=' + static_token;
		$('#opc_payment_methods-overlay').fadeIn('slow');

		if(typeof(orderOpcUrl) == "undefined")return;

		$.ajax({
			type: 'POST',
			url: orderOpcUrl,
			data: data,
			cache: false,
			dataType : "json",
			success: function(json) 
			{
				$('div#HOOK_TOP_PAYMENT').html(json.HOOK_TOP_PAYMENT);
				$('#opc_payment_methods-content div#HOOK_PAYMENT').html(json.HOOK_PAYMENT);
				$('#opc_payment_methods-overlay').fadeOut('slow');	
			}
		});
	}
		
    </script>        
<?php }} ?>
