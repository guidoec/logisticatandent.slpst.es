<?php /* Smarty version Smarty-3.1.19, created on 2015-06-19 11:53:46
         compiled from "/var/www/proxyfarma.com/themes/default-bootstrap/modules/homeslider/homeslider.tpl" */ ?>
<?php /*%%SmartyHeaderCode:20592603635583e6aa79e190-26383985%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '35120ef1dc7dec13fee62c29893a38c15125790e' => 
    array (
      0 => '/var/www/proxyfarma.com/themes/default-bootstrap/modules/homeslider/homeslider.tpl',
      1 => 1432566180,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '20592603635583e6aa79e190-26383985',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'page_name' => 0,
    'homeslider_slides' => 0,
    'slide' => 0,
    'link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_5583e6aa7d6545_46454488',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5583e6aa7d6545_46454488')) {function content_5583e6aa7d6545_46454488($_smarty_tpl) {?>
<?php if ($_smarty_tpl->tpl_vars['page_name']->value=='index') {?>
<!-- Module HomeSlider -->
    <?php if (isset($_smarty_tpl->tpl_vars['homeslider_slides']->value)) {?>
		<div id="homepage-slider">
			<?php if (isset($_smarty_tpl->tpl_vars['homeslider_slides']->value[0])&&isset($_smarty_tpl->tpl_vars['homeslider_slides']->value[0]['sizes'][1])) {?><?php $_smarty_tpl->_capture_stack[0][] = array('height', null, null); ob_start(); ?><?php echo $_smarty_tpl->tpl_vars['homeslider_slides']->value[0]['sizes'][1];?>
<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?><?php }?>
			<ul id="homeslider"<?php if (isset(Smarty::$_smarty_vars['capture']['height'])&&Smarty::$_smarty_vars['capture']['height']) {?> style="max-height:<?php echo Smarty::$_smarty_vars['capture']['height'];?>
px;"<?php }?>>
				<?php  $_smarty_tpl->tpl_vars['slide'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['slide']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['homeslider_slides']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['slide']->key => $_smarty_tpl->tpl_vars['slide']->value) {
$_smarty_tpl->tpl_vars['slide']->_loop = true;
?>
					<?php if ($_smarty_tpl->tpl_vars['slide']->value['active']) {?>
						<li class="homeslider-container">
							<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['url'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['legend'], ENT_QUOTES, 'UTF-8', true);?>
">
								<img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getMediaLink(((string)@constant('_MODULE_DIR_'))."homeslider/images/".((string)mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['image'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8')));?>
"<?php if (isset($_smarty_tpl->tpl_vars['slide']->value['size'])&&$_smarty_tpl->tpl_vars['slide']->value['size']) {?> <?php echo $_smarty_tpl->tpl_vars['slide']->value['size'];?>
<?php } else { ?> width="100%" height="100%"<?php }?> alt="<?php echo mb_convert_encoding(htmlspecialchars($_smarty_tpl->tpl_vars['slide']->value['legend'], ENT_QUOTES, 'UTF-8', true), "HTML-ENTITIES", 'UTF-8');?>
" />
							</a>
							<?php if (isset($_smarty_tpl->tpl_vars['slide']->value['description'])&&trim($_smarty_tpl->tpl_vars['slide']->value['description'])!='') {?>
								<div class="homeslider-description"><?php echo $_smarty_tpl->tpl_vars['slide']->value['description'];?>
</div>
							<?php }?>
						</li>
					<?php }?>
				<?php } ?>
			</ul>
		</div>
	<?php }?>

    <!-- /Ad-on-->

    <div id="testimonio">
      <div id="t-wrapper">
        <div id="testimonio-wrapper">
            <a href="http://proxyfarma.com/index.php?id_product=8&controller=product">
            <div id="testimonio-img"><img src="img/testimonio/alvaro.jpg"></div>
            <div id="testimonio-text"><p><strong style="font-size: 18px">"Siempre recomiendo viscoelástica a mis pacientes</strong>. Tanto en el colchón como en la almohada garantizan un buen descanso libre de presiones."</p><p style="text-align: right; color: #00A9E0"><strong style="font-size: 22px;">Alvaro Blein,<br><span style="font-size: 18px">Fisioterapeuta</span></strong></p></div>
            </a>
        </div>

          <div id="testimonio-wrapper2">
              <a href="http://www.proxyfarma.com/index.php?id_category=61&controller=category">
              <div id="testimonio-img2"><img src="img/testimonio/dr-garcia.png"></div>
              <div id="testimonio-text2"><p><strong style="font-size: 18px">"Para favorecer un buen desarrollo y</strong> funcionalidad del arco mandibular es importante que el chupete se adecue al espacio anatómico de la boca."</p><p style="text-align: right; color: #00A9E0"><strong style="font-size: 22px;">Dr. García Sala,<br>Pediatra</strong></p></div>
              </a>
          </div>
      </div>
    </div>

<!-- /Module HomeSlider -->
<?php }?><?php }} ?>
