<?php /* Smarty version Smarty-3.1.19, created on 2015-06-17 15:43:53
         compiled from "/var/www/proxyfarma.com/modules/pm_crosssellingoncart/pm_crosssellingoncart-16.tpl" */ ?>
<?php /*%%SmartyHeaderCode:187205676558179993f9466-51969708%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bbc5950632036bc98d965d6a9c0d69d61cc56f3c' => 
    array (
      0 => '/var/www/proxyfarma.com/modules/pm_crosssellingoncart/pm_crosssellingoncart-16.tpl',
      1 => 1432897378,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '187205676558179993f9466-51969708',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'csoc_product_selection' => 0,
    'csoc_prefix' => 0,
    'csoc_bloc_title' => 0,
    'imageSize' => 0,
    'cartProduct' => 0,
    'link' => 0,
    'PS_CATALOG_MODE' => 0,
    'PS_STOCK_MANAGEMENT' => 0,
    'restricted_country_mode' => 0,
    'priceDisplay' => 0,
    'add_prod_display' => 0,
    'static_token' => 0,
    'csoc_products_quantity' => 0,
    'csoc_order_page_link' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.19',
  'unifunc' => 'content_558179994c6625_81178429',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_558179994c6625_81178429')) {function content_558179994c6625_81178429($_smarty_tpl) {?><?php if (count($_smarty_tpl->tpl_vars['csoc_product_selection']->value)>0) {?>

<div id="csoc-container" class="<?php echo strtolower($_smarty_tpl->tpl_vars['csoc_prefix']->value);?>
">
	<?php if ($_smarty_tpl->tpl_vars['csoc_bloc_title']->value) {?><h2 class="page-subheading"><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['csoc_bloc_title']->value, ENT_QUOTES, 'UTF-8', true);?>
</h2><?php }?>

	<div id="<?php echo $_smarty_tpl->tpl_vars['csoc_prefix']->value;?>
" class="clearfix">
		<?php  $_smarty_tpl->tpl_vars['cartProduct'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['cartProduct']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['csoc_product_selection']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['cartProduct']->key => $_smarty_tpl->tpl_vars['cartProduct']->value) {
$_smarty_tpl->tpl_vars['cartProduct']->_loop = true;
?>
		<div class="product-container">
			<div class="left-block" style="width: <?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['getWidthSize'][0][0]->getWidth(array('type'=>$_smarty_tpl->tpl_vars['imageSize']->value),$_smarty_tpl);?>
px">
				<div class="product-image-container">
					<a class="product_img_link" href="<?php echo $_smarty_tpl->tpl_vars['link']->value->getProductLink($_smarty_tpl->tpl_vars['cartProduct']->value['id_product'],$_smarty_tpl->tpl_vars['cartProduct']->value['link_rewrite'],$_smarty_tpl->tpl_vars['cartProduct']->value['category']);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cartProduct']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
">
						<?php if (empty($_smarty_tpl->tpl_vars['cartProduct']->value['link_rewrite'])) {?>
							<img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink("default",$_smarty_tpl->tpl_vars['cartProduct']->value['id_image'],$_smarty_tpl->tpl_vars['imageSize']->value);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cartProduct']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" />
						<?php } else { ?>
							<img src="<?php echo $_smarty_tpl->tpl_vars['link']->value->getImageLink($_smarty_tpl->tpl_vars['cartProduct']->value['link_rewrite'],$_smarty_tpl->tpl_vars['cartProduct']->value['id_image'],$_smarty_tpl->tpl_vars['imageSize']->value);?>
" alt="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cartProduct']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
" />
						<?php }?>
						<?php if (isset($_smarty_tpl->tpl_vars['cartProduct']->value['new'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['new']==1) {?>
						<span class="new-box">
							<span class="new-label"><?php echo smartyTranslate(array('s'=>'New','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>
</span>
						</span>
						<?php }?>
						<?php if (isset($_smarty_tpl->tpl_vars['cartProduct']->value['on_sale'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['on_sale']&&isset($_smarty_tpl->tpl_vars['cartProduct']->value['show_price'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['show_price']&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
						<span class="sale-box">
							<span class="sale-label"><?php echo smartyTranslate(array('s'=>'Sale!','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>
</span>
						</span>
						<?php }?>
					</a>
				</div><!-- .product-image-container -->
			</div><!-- .left-block -->
			<div class="right-block">
				<h5>
					<?php if (isset($_smarty_tpl->tpl_vars['cartProduct']->value['pack_quantity'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['pack_quantity']) {?><?php echo (intval($_smarty_tpl->tpl_vars['cartProduct']->value['pack_quantity'])).(' x ');?>
<?php }?>
					<a class="product-name" href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cartProduct']->value['link'], ENT_QUOTES, 'UTF-8', true);?>
" title="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['cartProduct']->value['name'], ENT_QUOTES, 'UTF-8', true);?>
">
						<?php echo htmlspecialchars($_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_MODIFIER]['truncate'][0][0]->smarty_modifier_truncate($_smarty_tpl->tpl_vars['cartProduct']->value['name'],45,'...'), ENT_QUOTES, 'UTF-8', true);?>

					</a>
				</h5>
				<?php if ((!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&$_smarty_tpl->tpl_vars['PS_STOCK_MANAGEMENT']->value&&((isset($_smarty_tpl->tpl_vars['cartProduct']->value['show_price'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['show_price'])||(isset($_smarty_tpl->tpl_vars['cartProduct']->value['available_for_order'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['available_for_order'])))) {?>
				<div class="content_price">
					<?php if (isset($_smarty_tpl->tpl_vars['cartProduct']->value['show_price'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['show_price']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)) {?>
						<span class="price product-price">
							<?php if (!$_smarty_tpl->tpl_vars['priceDisplay']->value) {?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['cartProduct']->value['price']),$_smarty_tpl);?>
<?php } else { ?><?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['convertPrice'][0][0]->convertPrice(array('price'=>$_smarty_tpl->tpl_vars['cartProduct']->value['price_tax_exc']),$_smarty_tpl);?>
<?php }?>
						</span>
						<?php if (isset($_smarty_tpl->tpl_vars['cartProduct']->value['specific_prices'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['specific_prices']&&isset($_smarty_tpl->tpl_vars['cartProduct']->value['specific_prices']['reduction'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['specific_prices']['reduction']) {?>
							<span class="old-price product-price">
								<?php echo $_smarty_tpl->smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['displayWtPrice'][0][0]->displayWtPrice(array('p'=>$_smarty_tpl->tpl_vars['cartProduct']->value['price_without_reduction']),$_smarty_tpl);?>

							</span>
							<?php if ($_smarty_tpl->tpl_vars['cartProduct']->value['specific_prices']['reduction_type']=='percentage') {?>
								<span class="price-percent-reduction">-<?php echo $_smarty_tpl->tpl_vars['cartProduct']->value['specific_prices']['reduction']*100;?>
%</span>
							<?php }?>
						<?php }?>
					<?php } else { ?>
						<span class="price product-price">&nbsp;</span>
					<?php }?>
				</div>
				<?php }?>
				<div class="button-container">
					<?php if (($_smarty_tpl->tpl_vars['cartProduct']->value['id_product_attribute']==0||(isset($_smarty_tpl->tpl_vars['add_prod_display']->value)&&($_smarty_tpl->tpl_vars['add_prod_display']->value==1)))&&$_smarty_tpl->tpl_vars['cartProduct']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)&&$_smarty_tpl->tpl_vars['cartProduct']->value['minimal_quantity']<=1&&$_smarty_tpl->tpl_vars['cartProduct']->value['customizable']!=2&&!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value) {?>
						<?php if (($_smarty_tpl->tpl_vars['cartProduct']->value['allow_oosp']||$_smarty_tpl->tpl_vars['cartProduct']->value['quantity']>0)) {?>
							<?php if (isset($_smarty_tpl->tpl_vars['static_token']->value)) {?>
								<a class="button ajax_add_to_cart_button btn btn-default<?php if ($_smarty_tpl->tpl_vars['csoc_prefix']->value=='PM_MC_CSOC') {?> button-small<?php }?>" href="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['cartProduct']->value['id_product']);?>
<?php $_tmp8=ob_get_clean();?><?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['cartProduct']->value['id_product_attribute']);?>
<?php $_tmp9=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',false,null,"add=1&amp;id_product=".$_tmp8."&amp;id_product_attribute=".$_tmp9."&amp;token=".((string)$_smarty_tpl->tpl_vars['static_token']->value),false), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>
" data-id-product="<?php echo intval($_smarty_tpl->tpl_vars['cartProduct']->value['id_product']);?>
" data-id-product-attribute="<?php echo intval($_smarty_tpl->tpl_vars['cartProduct']->value['id_product_attribute']);?>
">
									<span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>
</span>
								</a>
							<?php } else { ?>
								<a class="button ajax_add_to_cart_button btn btn-default<?php if ($_smarty_tpl->tpl_vars['csoc_prefix']->value=='PM_MC_CSOC') {?> button-small<?php }?>" href="<?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['cartProduct']->value['id_product']);?>
<?php $_tmp10=ob_get_clean();?><?php ob_start();?><?php echo intval($_smarty_tpl->tpl_vars['cartProduct']->value['id_product_attribute']);?>
<?php $_tmp11=ob_get_clean();?><?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('cart',false,null,"add=1&amp;id_product=".$_tmp10."&amp;id_product_attribute=".$_tmp11,false), ENT_QUOTES, 'UTF-8', true);?>
" rel="nofollow" title="<?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>
" data-id-product="<?php echo intval($_smarty_tpl->tpl_vars['cartProduct']->value['id_product']);?>
" data-id-product-attribute="<?php echo intval($_smarty_tpl->tpl_vars['cartProduct']->value['id_product_attribute']);?>
">
									<span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>
</span>
								</a>
							<?php }?>
						<?php } else { ?>
							<span class="button ajax_add_to_cart_button btn btn-default<?php if ($_smarty_tpl->tpl_vars['csoc_prefix']->value=='PM_MC_CSOC') {?> button-small<?php }?> disabled">
								<span><?php echo smartyTranslate(array('s'=>'Add to cart','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>
</span>
							</span>
						<?php }?>
					<?php }?>
				</div>
				
				<?php if ((!$_smarty_tpl->tpl_vars['PS_CATALOG_MODE']->value&&((isset($_smarty_tpl->tpl_vars['cartProduct']->value['show_price'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['show_price'])||(isset($_smarty_tpl->tpl_vars['cartProduct']->value['available_for_order'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['available_for_order'])))) {?>
					<?php if (isset($_smarty_tpl->tpl_vars['cartProduct']->value['available_for_order'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['available_for_order']&&!isset($_smarty_tpl->tpl_vars['restricted_country_mode']->value)) {?>
						<span class="availability">
							<?php if (($_smarty_tpl->tpl_vars['cartProduct']->value['allow_oosp']||$_smarty_tpl->tpl_vars['cartProduct']->value['quantity']>0)) {?>
								<span class="available-now">
									<?php echo smartyTranslate(array('s'=>'In Stock','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>

								</span>
							<?php } elseif ((isset($_smarty_tpl->tpl_vars['cartProduct']->value['quantity_all_versions'])&&$_smarty_tpl->tpl_vars['cartProduct']->value['quantity_all_versions']>0)) {?>
								<span class="available-dif">
									<?php echo smartyTranslate(array('s'=>'Product available with different options','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>

								</span>
							<?php } else { ?>
								<span class="out-of-stock">
									<?php echo smartyTranslate(array('s'=>'Out of stock','mod'=>'pm_crosssellingoncart'),$_smarty_tpl);?>

								</span>
							<?php }?>
						</span>
					<?php }?>
				<?php }?>
			</div><!-- .right-block -->
		</div><!-- .product-container -->
		<?php } ?>
	</div>
</div>

<script>
	if (typeof($csocjqPm) == 'undefined') $csocjqPm = $;
	$csocjqPm(document).ready(function() {
		$csocjqPm("#<?php echo $_smarty_tpl->tpl_vars['csoc_prefix']->value;?>
").owlCarousel({
			items : <?php if (sizeof($_smarty_tpl->tpl_vars['csoc_product_selection']->value)<$_smarty_tpl->tpl_vars['csoc_products_quantity']->value) {?><?php echo sizeof($_smarty_tpl->tpl_vars['csoc_product_selection']->value);?>
<?php } else { ?><?php echo $_smarty_tpl->tpl_vars['csoc_products_quantity']->value;?>
<?php }?>,
			slideSpeed : 200,
			paginationSpeed : 800,
			autoPlay : true,
			stopOnHover : true,
			goToFirstSpeed : 1000,
			navigation : false,
			navigationText : ["prev","next"],
			scrollPerPage : true,
			pagination : true,
			baseClass : "owl-carousel",
			theme : "owl-theme",
			mouseDraggable : false,
			responsiveBaseWidth: <?php if ($_smarty_tpl->tpl_vars['csoc_prefix']->value=='PM_CSOC') {?>window<?php } else { ?>$csocjqPm('.nyroModalCont')<?php }?>
		});
		if (typeof(modalAjaxCart) == 'undefined' && typeof(ajaxCart) != 'undefined' && typeof(pm_reloadCartOnAdd) != 'undefined' && typeof(pm_csocLoopInterval) == 'undefined') {
			pm_csocLoopInterval = setInterval(function() {
				pm_reloadCartOnAdd('<?php echo $_smarty_tpl->tpl_vars['csoc_order_page_link']->value;?>
');
			}, 500);
		}
	});
</script>
<?php }?><?php }} ?>
