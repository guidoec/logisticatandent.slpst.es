<?php

class WebserviceRequest extends WebserviceRequestCore {
    public static function getResources(){
        $resources = parent::getResources();
        $resources['location'] = array('description' => 'Location for carriers', 'class' => 'Location');
        ksort($resources);
        return $resources;
    }

}

?>