<?php

class OrderDetail extends OrderDetailCore
{
    /** @var float */
    public $product_recomed_price;
    public $product_pharmacy_commission;

    public static $definition = array(
        'table' => 'order_detail',
        'primary' => 'id_order_detail',
        'fields' => array(
            'id_order' => 					array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_order_invoice' => 			array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_warehouse' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'id_shop' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId', 'required' => true),
            'product_id' => 				array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'product_attribute_id' =>		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'product_name' => 				array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true),
            'product_quantity' => 			array('type' => self::TYPE_INT, 'validate' => 'isInt', 'required' => true),
            'product_quantity_in_stock' => 	array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'product_quantity_return' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'product_quantity_refunded' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'product_quantity_reinjected' =>array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
            'product_price' => 				array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice', 'required' => true),
            'reduction_percent' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'reduction_amount' =>			array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'reduction_amount_tax_incl' =>  array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'reduction_amount_tax_excl' =>  array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'group_reduction' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'product_quantity_discount' => 	array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'product_ean13' => 				array('type' => self::TYPE_STRING, 'validate' => 'isEan13'),
            'product_upc' => 				array('type' => self::TYPE_STRING, 'validate' => 'isUpc'),
            'product_reference' => 			array('type' => self::TYPE_STRING, 'validate' => 'isReference'),
            'product_supplier_reference' => array('type' => self::TYPE_STRING, 'validate' => 'isReference'),
            'product_weight' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'tax_name' => 					array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'tax_rate' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'tax_computation_method' =>		array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
            'id_tax_rules_group' => 		array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'ecotax' => 					array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'ecotax_tax_rate' => 			array('type' => self::TYPE_FLOAT, 'validate' => 'isFloat'),
            'discount_quantity_applied' => 	array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'download_hash' => 				array('type' => self::TYPE_STRING, 'validate' => 'isGenericName'),
            'download_nb' => 				array('type' => self::TYPE_INT, 'validate' => 'isInt'),
            'download_deadline' => 			array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
            'unit_price_tax_incl' => 		array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'unit_price_tax_excl' => 		array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_price_tax_incl' => 		array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'total_price_tax_excl' => 		array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'purchase_supplier_price' => 	array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'original_product_price' => 	array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'product_recomed_price' =>      array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice'),
            'product_pharmacy_commission' =>      array('type' => self::TYPE_FLOAT, 'validate' => 'isPrice')
        ),
    );


    protected $webserviceParameters = array(
        'fields' => array (
            'id_order' => array('xlink_resource' => 'orders'),
            'product_id' => array('xlink_resource' => 'products'),
            'product_attribute_id' => array('xlink_resource' => 'combinations'),
            'product_quantity_reinjected' => array(),
            'group_reduction' => array(),
            'discount_quantity_applied' => array(),
            'download_hash' => array(),
            'download_deadline' => array()
        ),
        'hidden_fields' => array('tax_rate', 'tax_name'),
        'associations' => array(
            'taxes'  => array('resource' => 'tax', 'getter' => 'getWsTaxes', 'setter' => false,
                'fields' => array('id' =>  array(), ),
            ),
            'suppliers'  => array('resource' => 'supplier', 'getter' => 'getWsSuppliers', 'setter' => false,
                'fields' => array(
                    'id' =>  array(),
                    'product_supplier_reference' =>  array('setter' => false),
                ),
            ),
        ));

    public function getWsSuppliers()
    {
        $query = new DbQuery();
        $query->select(_DB_PREFIX_.'product_supplier.id_supplier  AS id,'._DB_PREFIX_.'product_supplier.product_supplier_reference');
        $query->from('order_detail');
        $query->join('JOIN '._DB_PREFIX_.'product_supplier on '._DB_PREFIX_.'product_supplier.id_product = '._DB_PREFIX_.'order_detail.product_id AND '._DB_PREFIX_.'product_supplier.id_product_attribute = '._DB_PREFIX_.'order_detail.product_attribute_id');
        $query->where('id_order_detail = '.(int)$this->id);
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query);
        return $result;
    }

    protected function setDetailProductPrice(Order $order, Cart $cart, $product)
    {
        $this->setContext((int)$product['id_shop']);
        Product::getPriceStatic((int)$product['id_product'], true, (int)$product['id_product_attribute'], 6, null, false, true, $product['cart_quantity'], false, (int)$order->id_customer, (int)$order->id_cart, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}, $specific_price, true, true, $this->context);
        $this->specificPrice = $specific_price;
        $this->original_product_price = Product::getPriceStatic($product['id_product'], false, (int)$product['id_product_attribute'], 6, null, false, false, 1, false, null, null, null, $null, true, true, $this->context);
        $this->product_price = $this->original_product_price;
        $this->unit_price_tax_incl = (float)$product['price_wt'];
        $this->unit_price_tax_excl = (float)$product['price'];
        $this->total_price_tax_incl = (float)$product['total_wt'];
        $this->total_price_tax_excl = (float)$product['total'];
        $this->product_recomed_price = (float)$product['recomed_price'];
        $this->product_pharmacy_commission = (float)$product['pharmacy_commission'];


        $this->purchase_supplier_price = (float)$product['wholesale_price'];
        if ($product['id_supplier'] > 0 && ($supplier_price = ProductSupplier::getProductPrice((int)$product['id_supplier'], $product['id_product'], $product['id_product_attribute'], true)) > 0)
            $this->purchase_supplier_price = (float)$supplier_price;

        $this->setSpecificPrice($order, $product);

        $this->group_reduction = (float)Group::getReduction((int)$order->id_customer);

        $shop_id = $this->context->shop->id;

        $quantity_discount = SpecificPrice::getQuantityDiscount((int)$product['id_product'], $shop_id,
            (int)$cart->id_currency, (int)$this->vat_address->id_country,
            (int)$this->customer->id_default_group, (int)$product['cart_quantity'], false, null, null, $null, true, true, $this->context);

        $unit_price = Product::getPriceStatic((int)$product['id_product'], true,
            ($product['id_product_attribute'] ? intval($product['id_product_attribute']) : null),
            2, null, false, true, 1, false, (int)$order->id_customer, null, (int)$order->{Configuration::get('PS_TAX_ADDRESS_TYPE')}, $null, true, true, $this->context);
        $this->product_quantity_discount = 0.00;
        if ($quantity_discount)
        {
            $this->product_quantity_discount = $unit_price;
            if (Product::getTaxCalculationMethod((int)$order->id_customer) == PS_TAX_EXC)
                $this->product_quantity_discount = Tools::ps_round($unit_price, 2);

            if (isset($this->tax_calculator))
                $this->product_quantity_discount -= $this->tax_calculator->addTaxes($quantity_discount['price']);
        }

        $this->discount_quantity_applied = (($this->specificPrice && $this->specificPrice['from_quantity'] > 1) ? 1 : 0);
    }
}