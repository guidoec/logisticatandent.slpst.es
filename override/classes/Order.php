<?php

class Order extends OrderCore
{
    protected $webserviceParameters = array(
        'objectMethods' => array('add' => 'addWs'),
        'objectNodeName' => 'order',
        'objectsNodeName' => 'orders',
        'fields' => array(
            'id_address_delivery' => array('xlink_resource'=> 'addresses'),
            'id_address_invoice' => array('xlink_resource'=> 'addresses'),
            'id_cart' => array('xlink_resource'=> 'carts'),
            'id_currency' => array('xlink_resource'=> 'currencies'),
            'id_lang' => array('xlink_resource'=> 'languages'),
            'id_customer' => array('xlink_resource'=> 'customers'),
            'id_carrier' => array('xlink_resource'=> 'carriers'),
            'current_state' => array(
                'xlink_resource'=> 'order_states',
                'getter' => 'getWsCurrentState',
                'setter' => 'setWsCurrentState'
            ),
            'module' => array('required' => true),
            'invoice_number' => array(),
            'invoice_date' => array(),
            'delivery_number' => array(),
            'delivery_date' => array(),
            'valid' => array(),
            'date_add' => array(),
            'date_upd' => array(),
            'shipping_number' => array(
                'getter' => 'getWsShippingNumber',
                'setter' => 'setWsShippingNumber'
            ),
        ),
        'associations' => array(
            'order_rows' => array('resource' => 'order_row', 'setter' => false, 'virtual_entity' => true,
                'fields' => array(
                    'id' =>  array(),
                    'product_id' => array('required' => true),
                    'product_attribute_id' => array('required' => true),
                    'product_quantity' => array('required' => true),
                    'product_name' => array('setter' => false),
                    'product_reference' => array('setter' => false),
                    'product_ean13' => array('setter' => false),
                    'product_upc' => array('setter' => false),
                    'product_price' => array('setter' => false),
                    'unit_price_tax_incl' => array('setter' => false),
                    'unit_price_tax_excl' => array('setter' => false),
                    'product_recomed_price' => array('setter' => false),
                    'product_pharmacy_commission' => array('setter' => false),
                    'product_supplier_id' => array('xlink_resource'=> 'suppliers'),
                    'product_supplier_reference' => array('setter' => false),
                )),
            'location' => array('resource' => 'location', 'setter' => false, 'virtual_entity' => true,
                'fields' => array(
                    'id' =>  array(),
                    'location' => array('required' => true),
                    'id_country' => array('xlink_resource'=> 'countries'),
                    'id_state' => array('xlink_resource'=> 'states'),
                    'address1' => array('setter' => false),
                    'postcode' => array('setter' => false),
                    'city' => array('setter' => false),
                    'description' => array('setter' => false),
                    'phone' => array('setter' => false),
                )),
            'payment' => array('resource' => 'order_payments', 'setter' => false, 'virtual_entity' => true,
                'fields' => array(
                    'id' =>  array(),
                    'order_reference' => array('setter' => false),
                    'id_currency' => array('xlink_resource'=> 'currencies'),
                    'amount' => array('setter' => false),
                    'payment_method' => array('setter' => false),
                    'conversion_rate' => array('setter' => false),
                    'transaction_id' => array('setter' => false),
                    'card_number' => array('setter' => false),
                    'card_brand' => array('setter' => false),
                    'card_expiration' => array('setter' => false),
                    'card_holder' => array('setter' => false),
                    'card_number' => array('setter' => false),
                )),
        ),

    );

    public function getWsLocation()
    {
        $query = '
			SELECT
            `'._DB_PREFIX_.'location`.id_location as id,
            `'._DB_PREFIX_.'location`.location,
            `'._DB_PREFIX_.'location`.id_country,
            `'._DB_PREFIX_.'location`.id_state,
            `'._DB_PREFIX_.'location`.address1,
            `'._DB_PREFIX_.'location`.postcode,
            `'._DB_PREFIX_.'location`.city,
            `'._DB_PREFIX_.'location`.description,
            phone
            FROM `'._DB_PREFIX_.'location`
            JOIN `'._DB_PREFIX_.'cart_location` ON `'._DB_PREFIX_.'cart_location`.id_location = `'._DB_PREFIX_.'location`.id_location
            WHERE `'._DB_PREFIX_.'cart_location`.id_cart = '.(int)$this->id_cart;
        $result = Db::getInstance()->executeS($query);
        return $result;
    }

    public function getWsPayment()
    {
        $query = '
			SELECT
            `'._DB_PREFIX_.'order_payment`.id_order_payment as id,
            `'._DB_PREFIX_.'order_payment`.order_reference,
            `'._DB_PREFIX_.'order_payment`.id_currency,
            `'._DB_PREFIX_.'order_payment`.amount,
            `'._DB_PREFIX_.'order_payment`.payment_method,
            `'._DB_PREFIX_.'order_payment`.conversion_rate,
            `'._DB_PREFIX_.'order_payment`.transaction_id,
            `'._DB_PREFIX_.'order_payment`.card_number,
            `'._DB_PREFIX_.'order_payment`.card_brand,
            `'._DB_PREFIX_.'order_payment`.card_expiration,
            `'._DB_PREFIX_.'order_payment`.card_holder,
            `'._DB_PREFIX_.'order_payment`.card_number
            FROM `'._DB_PREFIX_.'order_payment`
            JOIN `'._DB_PREFIX_.'order_invoice_payment` ON `'._DB_PREFIX_.'order_invoice_payment`.id_order_payment = `'._DB_PREFIX_.'order_payment`.id_order_payment
            WHERE `'._DB_PREFIX_.'order_invoice_payment`.id_order = '.(int)$this->id;
        $result = Db::getInstance()->executeS($query);
        return $result;
    }

    public function getWsOrderRows()
    {
        $query = '
			SELECT
			`id_order_detail` as `id`,
			`product_id`,
			`product_price`,
			`id_order`,
			`product_attribute_id`,
			`product_quantity`,
			`product_name`,
			`product_reference`,
			`product_ean13`,
			`product_upc`,
			`unit_price_tax_incl`,
			`unit_price_tax_excl`,
			`product_recomed_price`,
			`product_pharmacy_commission`,
			'._DB_PREFIX_.'product_supplier.id_supplier product_supplier_id,
			'._DB_PREFIX_.'product_supplier.product_supplier_reference
			FROM `'._DB_PREFIX_.'order_detail`
			LEFT JOIN '._DB_PREFIX_.'product_supplier on '._DB_PREFIX_.'product_supplier.id_product = '._DB_PREFIX_.'order_detail.product_id AND '._DB_PREFIX_.'product_supplier.id_product_attribute = '._DB_PREFIX_.'order_detail.product_attribute_id
			WHERE id_order = '.(int)$this->id;
        $result = Db::getInstance()->executeS($query);
        return $result;
    }
}