<?php
class Location extends ObjectModel {

    public $id_location;
    public $location;
    public $id_country;
    public $id_state;
    public $address1;
    public $postcode;
    public $city;
    public $description;
    public $phone;

    public static $definition = array(
        'table' => 'location',
        'primary' => 'id_location',
        'fields' => array(
            'id_location' => array('type' => self::TYPE_INT, 'required' => true),
            'location' => array('type' => self::TYPE_STRING, 'required' => true),
            'id_country' => array('type' => self::TYPE_INT, 'required' => true),
            'id_state' => array('type' => self::TYPE_INT, 'required' => true),
            'address1' => array('type' => self::TYPE_STRING, 'required' => true),
            'postcode' => array('type' => self::TYPE_INT, 'required' => true),
            'city' => array('type' => self::TYPE_STRING, 'required' => true),
            'description' => array('type' => self::TYPE_STRING, 'required' => true),
            'phone' => array('type' => self::TYPE_STRING, 'required' => true),
        )
    );

    protected $webserviceParameters = array(
        'objectsNodeName' => 'addresses',
        'fields' => array(
            'id_country' => array('xlink_resource'=> 'countries'),
            'id_state' => array('xlink_resource'=> 'states'),
        ),
    );

}
?>