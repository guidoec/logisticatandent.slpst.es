<?php

Product::$definition['fields']['recomed_price'] = array('type' => ObjectModel::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice');
Product::$definition['fields']['pharmacy_commission'] = array('type' => ObjectModel::TYPE_FLOAT, 'shop' => true, 'validate' => 'isPrice');

class Product extends ProductCore
{

    public $recomed_price = 0;
    public $pharmacy_commission = 0;



    protected $webserviceParameters = array(
        'objectMethods' => array(
            'add' => 'addWs',
            'update' => 'updateWs'
        ),
        'objectNodeNames' => 'products',
        'fields' => array(
            'id_manufacturer' => array(
                'xlink_resource' => 'manufacturers'
            ),
            'id_supplier' => array(
                'xlink_resource' => 'suppliers'
            ),
            'id_category_default' => array(
                'xlink_resource' => 'categories'
            ),
            'new' => array(),
            'cache_default_attribute' => array(),
            'id_default_image' => array(
                'getter' => 'getCoverWs',
                'setter' => 'setCoverWs',
                'xlink_resource' => array(
                    'resourceName' => 'images',
                    'subResourceName' => 'products'
                )
            ),
            'id_default_combination' => array(
                'getter' => 'getWsDefaultCombination',
                'setter' => 'setWsDefaultCombination',
                'xlink_resource' => array(
                    'resourceName' => 'combinations'
                )
            ),
            'id_tax_rules_group' => array(
                'xlink_resource' => array(
                    'resourceName' => 'tax_rule_groups'
                )
            ),
            'position_in_category' => array(
                'getter' => 'getWsPositionInCategory',
                'setter' => 'setWsPositionInCategory'
            ),
            'manufacturer_name' => array(
                'getter' => 'getWsManufacturerName',
                'setter' => false
            ),
            'quantity' => array(
                'getter' => false,
                'setter' => false
            ),
            'type' => array(
                'getter' => 'getWsType',
                'setter' => 'setWsType',
            ),
        ),
        'associations' => array(
            'categories' => array(
                'resource' => 'category',
                'fields' => array(
                    'id' => array('required' => true),
                )
            ),
            'images' => array(
                'resource' => 'image',
                'fields' => array('id' => array())
            ),
            'combinations' => array(
                'resource' => 'combination',
                'fields' => array(
                    'id' => array('required' => true),
                )
            ),
            'product_option_values' => array(
                'resource' => 'product_option_value',
                'fields' => array(
                    'id' => array('required' => true),
                )
            ),
            'product_features' => array(
                'resource' => 'product_feature',
                'fields' => array(
                    'id' => array('required' => true),
                    'id_feature_value' => array(
                        'required' => true,
                        'xlink_resource' => 'product_feature_values'
                    ),
                )
            ),
            'tags' => array('resource' => 'tag',
                'fields' => array(
                    'id' => array('required' => true),
                )),
            'stock_availables' => array('resource' => 'stock_available',
                'fields' => array(
                    'id' => array('required' => true),
                    'id_product_attribute' => array('required' => true),
                ),
                'setter' => false
            ),
            'accessories' => array(
                'resource' => 'product',
                'api' => 'products',
                'fields' => array(
                    'id' => array(
                        'required' => true,
                        'xlink_resource' => 'product'),
                )
            ),
            'product_bundle' => array(
                'resource' => 'product',
                'api' => 'products',
                'fields' => array(
                    'id' => array('required' => true),
                    'quantity' => array(),
                ),
            ),
            'suppliers' => array(
                'resource' => 'supplier',
                'fields' => array(
                    'id' => array('required' => true),
                    'product_supplier_reference' => array('setter' => false),
                    'product_supplier_price_te' => array('setter' => false),
                )
            ),
            /*
            'recomed_price' => array(
                'resource' => 'recomed_price',
                'fields' => array(
                    'id' => array('required' => true),
                    'recomed_price' => array('setter' => false),
                )
            ),
            */
        ),
    );

    public function getWsSuppliers()
    {
        $query =
            'SELECT
            `'._DB_PREFIX_.'supplier`.id_supplier AS id,
            `'._DB_PREFIX_.'product_supplier`.product_supplier_reference,
            `'._DB_PREFIX_.'product_supplier`.product_supplier_price_te
            FROM `'._DB_PREFIX_.'supplier`
            JOIN `'._DB_PREFIX_.'product_supplier` ON `'._DB_PREFIX_.'product_supplier`.id_supplier = `'._DB_PREFIX_.'supplier`.id_supplier
            WHERE `'._DB_PREFIX_.'product_supplier`.id_product = '.(int)$this->id;
        $result = Db::getInstance()->executeS($query);
        return $result;
    }
    /*
    public function getWsRecomedPrice()
    {
        $query =
            'SELECT
            `'._DB_PREFIX_.'extra_fieldsmodule`.id_extra_fieldsmodule AS id,
            `'._DB_PREFIX_.'extra_fieldsmodule`.textarea as recomed_price
            FROM `'._DB_PREFIX_.'extra_fieldsmodule`
            WHERE `'._DB_PREFIX_.'extra_fieldsmodule`.id_product = '.(int)$this->id;
        $result = Db::getInstance()->executeS($query);
        return $result;
    }
    */
}