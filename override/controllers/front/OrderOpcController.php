<?php








class OrderOpcController extends OrderOpcControllerCore

{

	/*
	* module: agilepickupcenter
	* date: 2015-06-25 10:08:57
	* version: 1.4.2.0
	*/
	protected function _getPaymentMethods()

	{

		global $cart;

				if(Module::isInstalled('agilepickupcenter') AND $cart->id_carrier == intval(Configuration::getGlobalValue('AGILE_PICKUPCENTER_CARRIER_ID')))

		{

	        include_once(_PS_ROOT_DIR_  . "/modules/agilepickupcenter/Location.php");

		

	        $cartLocationInfo = Location::getCartLocationInfo($cart->id);

	        $id_location = 0;

	        if(isset($cartLocationInfo) AND intval($cartLocationInfo['id_location']) > 0)$id_location = intval($cartLocationInfo['id_location']);

			if($id_location <= 0)return '<p class="warning">'.Tools::displayError('Please choose pickup location').'</p>';

		}

		

		return parent::_getPaymentMethods();

	}    

}

